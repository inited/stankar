#!/usr/bin/env bash

inited prepare android clean
cordova plugin add ./libs/phonegap-plugin-push
cordova plugin add ./libs/com.synconset.imagepicker --variable PHOTO_LIBRARY_USAGE_DESCRIPTION="Přidávání obrázků k novým akcím"
cp google-services.json platforms/android/app/google-services.json
