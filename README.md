## stankar

### Vytváření eventů

Při přidávání taxonomy termínů jsem našel chybu, teda navím jestli je to chyba ale nenašel jsem jiný způsob jak to vyřešit

Pokud se posílá na web post, kterému se nastavuje taxonomy term a posílá se pouze jeden, je nutné poslat jeho slug.

Naopak pokud se jich posílá víc a posílají se postem jako pole, je třeba poslat jejich id

viz. PHP funkce:
```php
wp_set_post_terms((int)$this->id, $value, str_replace($taxonomyStr, "", $key), true);
```

$value je hodnota která se posílá v postu, buď pole id nebo string obsahující slug taxonomy termu