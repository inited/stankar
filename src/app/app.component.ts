import { Component, ViewChild } from "@angular/core";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { AlertController, Events, MenuController, Nav, Platform, Toggle } from "ionic-angular";

import { InAppBrowser } from "@ionic-native/in-app-browser";
import { ArticlesPage } from "../pages/articles/articles";
import { NotificationsProvider } from "../providers/notifications-provider";
import { UsersProvider } from "../providers/users-provider";


@Component({
    templateUrl: "app.html"
})
export class MyApp {
    @ViewChild(Nav)
    public nav: Nav;

    public rootPage: any = ArticlesPage;
    public isLoggedIn: boolean;
    public userRoles: Array<string>;
    public userLevels: Array<string>;
    public username: string;
    public notificationsEnabled: boolean;
    public numberOfFavorite: number = 0;

    public pages: Array<{ title: string, component: string, loginRequest: boolean, roleRequest: boolean, numberOfFavorite: number }>;

    private alertOpened: boolean;

    constructor(public platform: Platform,
                public usersProv: UsersProvider,
                private splashScreen: SplashScreen,
                private statusBar: StatusBar,
                private notificationsProv: NotificationsProvider,
                private alertCtrl: AlertController,
                private menuCtrl: MenuController,
                private inAppBrowser: InAppBrowser,
                private events: Events) {
        this.initializeApp();
        this.usersProv.onLogin.subscribe(() => {
            this.userLoggedIn();
        });
        this.usersProv.onLogout.subscribe(() => {
            this.userLoggedOut();
        });
        this.usersProv.init().then((res: any) => {
            this.notificationsProv.init(this.nav);
        }).catch((res: any) => {
            this.notificationsProv.init(this.nav);
        });

        this.events.subscribe("favorite:change", (numberOfFav) => {
            console.log("changed favorite:change app component", numberOfFav);
            this.numberOfFavorite = numberOfFav;
        });

        this.notificationsEnabled = localStorage.getItem("notificationsDisabled") != "true";

        // used for an example of ngFor and navigation
        this.pages = [
            {title: "Příspěvky", component: "ArticlesPage", loginRequest: false, roleRequest: false, numberOfFavorite: -1},
            {title: "Oblíbené příspěvky", component: "FavoritePage", loginRequest: true, roleRequest: false, numberOfFavorite: 1},
            {title: "Napsat příspěvek", component: "NewArticlePage", loginRequest: true, roleRequest: false, numberOfFavorite: -1},
            {title: "Kalendář akcí", component: "CalendarPage", loginRequest: true, roleRequest: false, numberOfFavorite: -1},
            {title: "Přidat akci", component: "NewEventPage", loginRequest: true, roleRequest: true, numberOfFavorite: -1},
            {title: "Tip na akci", component: "EventTipPage", loginRequest: false, roleRequest: false, numberOfFavorite: -1},
            {title: "Aktuálně o Stánkař.cz", component: "NewsPage", loginRequest: false, roleRequest: false, numberOfFavorite: -1}
        ];


    }

    public userLoggedIn(): void {
        console.log("App got loggedIn");
        this.isLoggedIn = true;
        this.username = this.usersProv.user.nickname;
        this.userRoles = this.usersProv.user.roles;
        this.userLevels = this.usersProv.user.levels;
    }

    public userLoggedOut(): void {
        console.log("App got not logged in");
        this.isLoggedIn = false;
        this.userRoles = ["subscriber"];
        this.userLevels = [];
        this.notificationsProv.register();
    }

    public initializeApp(): void {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            if (this.platform.is("ios")) {
                this.statusBar.styleDefault();
            }
            this.platform.registerBackButtonAction(() => {
                this.backButtonClicked();
            });
        }).catch((res: any) => {
            console.log("Not cordova");
        });
    }

    public askUs(): void {
        this.inAppBrowser.create("https://stankar.cz/zeptejte-se-nas/", "_system");
    }

    public logout(): void {
        this.usersProv.logout();
        this.nav.setRoot(ArticlesPage);
    }

    public openPage(page: {"type": string, "component": string, loginRequest: boolean, roleRequest: boolean}): void {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    }

    public goToLogin(): void {
        this.nav.push("LoginPage");
    }

    public alertNotLoggedIn(): void {
        this.alertCtrl.create({
            title: "Příhlášení",
            message: "Pro zobrazení kalendáře akcí se musíte přihlásit",
            buttons: [
                {
                    text: "Přihlášení",
                    handler: (): void => {
                        this.goToLogin();
                        this.menuCtrl.close();
                    }
                },
                {
                    text: "Zrušit"
                }
            ]
        }).present();
    }

    public notificationsChanged(toggle: Toggle): void {
        this.notificationsEnabled = toggle.checked;
        if (!this.notificationsEnabled) {
            localStorage.setItem("notificationsDisabled", "true");
            this.notificationsProv.unregister();
        } else {
            localStorage.removeItem("notificationsDisabled");
            this.notificationsProv.init();
        }
    }

    private backButtonClicked(): void {
        console.log(this.nav);
        if (this.nav.last() != this.nav.first()) {
            this.nav.last().getNav().pop();
        } else {
            if (this.menuCtrl.isOpen()) {
                if (!this.alertOpened) {
                    this.alertOpened = true;
                    this.alertCtrl.create({
                        title: "Ukončení",
                        message: "Opravdu chcete ukončit aplikaci?",
                        buttons: [
                            {
                                text: "Ne",
                                role: "cancel",
                                handler: () => {
                                    this.alertOpened = false;
                                }
                            },
                            {
                                text: "Ano",
                                handler: () => {
                                    this.platform.exitApp();
                                }
                            }
                        ]
                    }).present();
                }
            } else {
                this.menuCtrl.open();
            }
        }
    }
}
