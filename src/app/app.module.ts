import { ErrorHandler, LOCALE_ID, NgModule } from "@angular/core";
import { HttpModule } from "@angular/http";
import { BrowserModule } from "@angular/platform-browser";
import { Device } from "@ionic-native/device";
import { FileTransfer } from "@ionic-native/file-transfer";
import { ImagePicker } from "@ionic-native/image-picker";
import { Push } from "@ionic-native/push";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { IonicStorageModule } from "@ionic/storage";
import "intl";
import "intl/locale-data/jsonp/en";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";

import cs from "@angular/common/locales/cs";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { ArticlePopoverComponent } from "../components/article-popover/article-popover";
import { IonDatepickerModule } from "../components/ion-datepicker/ion-datepicker.module";
import { ArticlesPageModule } from "../pages/articles/articles.module";
import { Cf7dbProvider } from "../providers/cf7db-provider";
import { NotificationsProvider } from "../providers/notifications-provider";
import { PageProvider } from "../providers/page-provider";
import { PostsProvider } from "../providers/posts-provider";
import { TermsProvider } from "../providers/terms-provider";
import { UsersProvider } from "../providers/users-provider";
import { UtilsProvider } from "../providers/utils-provider";
import { MyApp } from "./app.component";

import { registerLocaleData } from "@angular/common";

registerLocaleData(cs);

//export const BaseUrl: string = "http://stankar.inited.cz/";
export const BaseUrl: string = "https://stankar.cz/";
export const ApiUrl: string = BaseUrl + "api/";

@NgModule({
    declarations: [
        MyApp,
        ArticlePopoverComponent
    ],
    imports: [
        BrowserModule,
        HttpModule,
        IonicModule.forRoot(MyApp, {
            pageTransition: "md-transition"
        }),
        IonicStorageModule.forRoot({
            name: "__stankarDB",
            driverOrder: ["sqlite", "websql", "indexeddb"]
        }),
        IonDatepickerModule,
        ArticlesPageModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        ArticlePopoverComponent
    ],
    providers: [
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        {provide: LOCALE_ID, useValue: "cs"},
        //Plugins
        Device,
        Push,
        SplashScreen,
        StatusBar,
        ImagePicker,
        InAppBrowser,
        FileTransfer,
        //Providers,
        NotificationsProvider,
        PostsProvider,
        UsersProvider,
        TermsProvider,
        Cf7dbProvider,
        UtilsProvider,
        PageProvider
    ]
})
export class AppModule {
}
