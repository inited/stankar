import { Pipe } from "@angular/core";

/*
 Generated class for the Truncate pipe.

 See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 Angular 2 Pipes.
 */

@Pipe({
    name: "truncate"
})
export class TruncatePipe {
    public transform(value: string, args: string): string {
        // let limit = args.length > 0 ? parseInt(args[0], 10) : 10;
        // let trail = args.length > 1 ? args[1] : '...';
        let limit: number = args ? parseInt(args, 10) : 10;
        let trail: string = "...";

        return value.length > limit ? value.substring(0, limit) + trail : value;
    }
}
