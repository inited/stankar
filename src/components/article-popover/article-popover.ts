import { Component, Input } from "@angular/core";
import { AlertController, Loading, LoadingController, NavController, ViewController } from "ionic-angular";
import { PostsProvider } from "../../providers/posts-provider";
import { Article } from "../../model/Article";

/**
 * Generated class for the ArticlePopoverComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
    selector: "article-popover",
    templateUrl: "article-popover.html"
})
export class ArticlePopoverComponent {

    private article: Article;
    private navCtrl: NavController;
    private postType: string;

    constructor(private postsProv: PostsProvider,
                private alertCtrl: AlertController,
                private loadingCtrl: LoadingController,
                private viewCtrl: ViewController) {
        this.article = viewCtrl.getNavParams().get("article");
        this.navCtrl = viewCtrl.getNavParams().get("navCtrl");
        this.postType = this.viewCtrl.getNavParams().get("postType");
    }

    public deleteArticle(): void {
        this.alertCtrl.create({
            title: "Jste si jistý?",
            message: "Opravdu chcete odstranit tento příspěvek?",
            buttons: [
                {
                    text: "Zrušit"
                },
                {
                    text: "OK",
                    handler: (): void => {
                        let loading: Loading = this.loadingCtrl.create({
                            content: "Odstraňuji příspěvek"
                        });
                        loading.present();
                        this.postsProv.deletePost(this.article.id, this.postType).then((res: any) => {
                            loading.dismiss();
                            this.alertCtrl.create({
                                title: "Hotovo",
                                message: "Příspěvek byl úspěšně odstraněn.",
                                buttons: [
                                    {
                                        text: "OK",
                                        handler: (): void => {
                                            this.viewCtrl.dismiss();
                                            this.navCtrl.setRoot("ArticlesPage");
                                        }
                                    }
                                ],
                                enableBackdropDismiss: false
                            }).present();
                        }).catch((res: any) => {
                            loading.dismiss();
                            this.alertCtrl.create({
                                title: "Chyba",
                                message: "Nastala chyba při odstraňování příspěvku, zkontrolujte, že jste přihlášený jako administrátor a zkuste to znovu.",
                                buttons: ["OK"],
                                enableBackdropDismiss: false
                            }).present();
                        });
                    }
                }
            ],
            enableBackdropDismiss: false
        }).present();
    }

    public deactivate(): void {
        this.alertCtrl.create({
            title: "Jste si jistý?",
            message: "Opravdu chcete deaktivovat tento příspěvek?",
            buttons: [
                {
                    text: "Zrušit"
                },
                {
                    text: "OK",
                    handler: (): void => {
                        let loading: Loading = this.loadingCtrl.create({
                            content: "Deaktivuji příspěvek"
                        });
                        loading.present();
                        this.postsProv.deactivatePost(this.article.id, this.postType).then((res: any) => {
                            loading.dismiss();
                            this.alertCtrl.create({
                                title: "Hotovo",
                                message: "Příspěvek byl úspěšně deaktivován.",
                                buttons: [
                                    {
                                        text: "OK",
                                        handler: (): void => {
                                            this.viewCtrl.dismiss();
                                            this.navCtrl.setRoot("ArticlesPage");
                                        }
                                    }
                                ],
                                enableBackdropDismiss: false
                            }).present();
                        }).catch((res: any) => {
                            loading.dismiss();
                            this.alertCtrl.create({
                                title: "Chyba",
                                message: "Nastala chyba při deaktivování příspěvku, zkontrolujte, že jste přihlášený jako administrátor a zkuste to znovu.",
                                buttons: ["OK"],
                                enableBackdropDismiss: false
                            }).present();
                        });
                    }
                }
            ],
            enableBackdropDismiss: false
        }).present();
    }

}
