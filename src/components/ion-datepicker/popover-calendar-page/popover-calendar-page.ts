import { Component } from "@angular/core";
import { ViewController } from "ionic-angular";

import { UtilsProvider } from "../../../providers/utils-provider";

/**
 * Generated class for the PopoverCalendarPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
    selector: "page-popover-calendar-page",
    templateUrl: "popover-calendar-page.html",
})
export class PopoverCalendarPage {

    public eventSource: Array<{title: string, startTime: Date, endTime: Date, allDay: boolean}> = [];
    public event: {title: string, startTime: Date, endTime: Date, allDay: boolean} = undefined;
    public showEventDetail: boolean = false;
    public currentDate: Date = new Date();
    public lastCurrentDate: Date = new Date();
    public isBeginEnd: boolean = false;
    public firstSelect: boolean = true;
    public firstChange: boolean = true;
    public dateType: string = "begin";
    public selectedDateLabel: string = "Selected date:";
    public dateFormat: string = "shortDate";
    public beginLabel: string = "Begin";
    public endLabel: string = "End";
    public cancelButton: string = "Cancel";
    public okButton: string = "OK";

    private alreadySelected: boolean = false;

    constructor(private viewCtrl: ViewController, utilsProv: UtilsProvider) {
        if (utilsProv.isDate(viewCtrl.data.date)) {
            this.setSimpleDate(viewCtrl.data.date);
        } else if (utilsProv.isDatesObject(viewCtrl.data.date)) {
            this.setBeginEndDate(viewCtrl.data.date);
        }
        this.eventSource.push(this.event);
        this.setLabelsAndFormat(viewCtrl.data);
    }

    public ionViewDidLoad(): void {
        console.log("ionViewDidLoad PopoverCalendarPage");
    }

    public currentDateChanged(date: Date): void {
        if (this.lastCurrentDate.getDate() == date.getDate() && (this.lastCurrentDate.getMonth() == date.getMonth() + 1 || this.lastCurrentDate.getMonth() == date.getMonth() - 1)) {
            this.lastCurrentDate = date;
            return;
        }

        if (this.alreadySelected) {
            return;
        }
        this.alreadySelected = true;
        setTimeout(() => {
            this.alreadySelected = false;
        }, 300);

        this.lastCurrentDate = date;
        this.currentDate = date;
        if (!this.isBeginEnd) {
            this.event.startTime = this.currentDate;
            this.event.endTime = this.currentDate;
        } else {
            console.log("Switching date type");
            switch (this.dateType) {
            case "begin":
                this.setBegin(date);
                break;
            case "end":
                this.setEnd(date);
                break;
            }
        }
    }

    public dateTypeChanged(): void {
        this.firstSelect = false;
    }

    public ok(): void {
        if (!this.isBeginEnd) {
            this.viewCtrl.dismiss(this.currentDate);
        } else {
            this.viewCtrl.dismiss({
                beginDate: this.event.startTime,
                endDate: this.event.endTime
            });
        }
    }

    public cancel(): void {
        this.viewCtrl.dismiss(undefined);
    }

    private setLabelsAndFormat(data: any): void {
        if (data.selectedDateLabel) {
            this.selectedDateLabel = data.selectedDateLabel;
        }
        if (data.dateFormat) {
            this.dateFormat = data.dateFormat;
        }
        if (data.beginLabel) {
            this.beginLabel = data.beginLabel;
        }
        if (data.endLabel) {
            this.endLabel = data.endLabel;
        }
        if (data.cancelButton) {
            this.cancelButton = data.cancelButton;
        }
        if (data.okButton) {
            this.okButton = data.okButton;
        }
    }

    private setSimpleDate(date: Date): void {
        this.isBeginEnd = false;
        this.currentDate = date;
        this.lastCurrentDate = this.currentDate;
        this.event = {
            title: "Selected Date",
            startTime: this.currentDate,
            endTime: this.currentDate,
            allDay: false
        };
    }

    private setBeginEndDate(dates: {beginDate: Date, endDate: Date}): void {
        this.isBeginEnd = true;
        this.currentDate = dates.beginDate;
        this.lastCurrentDate = this.currentDate;
        this.event = {
            title: "Selected Date",
            startTime: new Date(this.currentDate),
            endTime: new Date(dates.endDate),
            allDay: false
        };
    }

    private setBegin(date: Date): void {
        this.event.startTime = new Date(date);
        if (!this.firstChange && this.firstSelect) {
            this.dateType = "end";
        }
        if (this.firstChange) {
            this.firstChange = false;
        }
        if (this.event.startTime > this.event.endTime) {
            this.event.endTime = this.event.startTime;
        }
    }

    private setEnd(date: Date): void {
        this.event.endTime = new Date(date);
        if (this.event.startTime > this.event.endTime) {
            this.event.startTime = this.event.endTime;
        }
    }

}
