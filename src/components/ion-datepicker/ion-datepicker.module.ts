import { NgModule } from "@angular/core";
import { IonicModule } from "ionic-angular";
import { NgCalendarModule } from "ionic2-calendar";
import { IonDatepicker } from "./ion-datepicker";

import { PopoverCalendarPage } from "./popover-calendar-page/popover-calendar-page";

@NgModule({
    declarations: [
        IonDatepicker,
        PopoverCalendarPage
    ],
    imports: [
        IonicModule,
        NgCalendarModule
    ],
    exports: [
        IonDatepicker
    ],
    entryComponents: [
        PopoverCalendarPage
    ]
})
export class IonDatepickerModule {
}
