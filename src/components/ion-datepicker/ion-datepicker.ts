import { Component, forwardRef, Input } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
import { Popover, PopoverController } from "ionic-angular";

import { UtilsProvider } from "../../providers/utils-provider";
import { PopoverCalendarPage } from "./popover-calendar-page/popover-calendar-page";

const noop: Function = (): void => {
};

export const ION_DATEPICKER_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => IonDatepicker),
    multi: true
};

/**
 * Generated class for the IonDatepicker component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
    selector: "ion-datepicker",
    templateUrl: "ion-datepicker.html",
    providers: [ION_DATEPICKER_CONTROL_VALUE_ACCESSOR]
})
export class IonDatepicker implements ControlValueAccessor {

    @Input("label")
    public label: string;
    @Input("placeholder")
    public placeholder: string;
    @Input("selectedDateLabel")
    public selectedDateLabel: string;
    @Input("dateFormat")
    public dateFormat: string = "shortDate";
    @Input("beginLabel")
    public beginLabel: string;
    @Input("endLabel")
    public endLabel: string;
    @Input("cancelButton")
    public cancelButton: string;
    @Input("okButton")
    public okButton: string;
    public buttonDisabled: boolean = false;

    public isDate: Function;
    public isDatesObject: Function;

    private innerValue: Date | { beginDate: Date, endDate: Date };
    private onTouchedCallback: Function = noop;
    private onChangeCallback: Function = noop;

    get value(): Date | { beginDate: Date, endDate: Date } {
        return this.innerValue;
    }

    set value(v: Date | { beginDate: Date, endDate: Date }) {
        if (v !== this.innerValue) {
            this.innerValue = v;
            this.onChangeCallback(v);
        }
    }

    constructor(private popoverCtrl: PopoverController, private utilsProv: UtilsProvider) {
        console.log("Hello IonDatepicker Component");
        this.isDate = utilsProv.isDate;
        this.isDatesObject = utilsProv.isDatesObject;
    }

    public writeValue(value: any): void {
        if (value !== this.innerValue) {
            this.innerValue = value;
        }
    }

    public registerOnChange(fn: any): void {
        this.onChangeCallback = fn;
    }

    public registerOnTouched(fn: any): void {
        this.onTouchedCallback = fn;
    }

    public setDisabledState(isDisabled: boolean): void {
        this.buttonDisabled = isDisabled;
    }

    public showDatepicker(): void {
        let popover: Popover = this.popoverCtrl.create(PopoverCalendarPage, {
            date: this.value ? this.value : new Date(),
            selectedDateLabel: this.selectedDateLabel,
            dateFormat: this.dateFormat,
            beginLabel: this.beginLabel,
            endLabel: this.endLabel,
            cancelButton: this.cancelButton,
            okButton: this.okButton
        }, {
            cssClass: "ion-datepicker-popover"
        });
        popover.onDidDismiss((res: Date) => {
            if (res) {
                this.value = res;
            }
        });
        popover.present();
    }

}
