import { Injectable, NgZone } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Device } from "@ionic-native/device";
import { NotificationEventResponse, Push, PushObject, RegistrationEventResponse } from "@ionic-native/push";
import { Storage } from "@ionic/storage";
import { Nav } from "ionic-angular";
import "rxjs/add/operator/map";
import { ApiUrl } from "../app/app.module";
import { ArticlesPage } from "../pages/articles/articles";
import { UsersProvider } from "./users-provider";

/*
 Generated class for the NotificationsProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class NotificationsProvider {

    public static availablePost: { type: string, id: number };

    private _token: string;
    private _nav: Nav;

    private registrationId: string;
    private pushObject: PushObject;

    public get token(): string | Promise<string> {
        return new Promise((resolve: any, reject: any): void => {
            if (this._token && this._token != "") {
                resolve(this._token);
            } else {
                this.storage.get("pntoken").then(resolve).catch(reject);
            }
        });
    }

    public set token(value: string | Promise<string>) {
        if (value instanceof Promise) {
            value.then((token: string) => {
                this._token = token;
                this.storage.set("pntoken", token);
            });
        } else {
            this._token = value;
            this.storage.set("pntoken", value);
        }
    }

    constructor(public http: Http, private push: Push, private device: Device, private storage: Storage, private usersProv: UsersProvider, private ngZone: NgZone) {
        console.log("Hello NotificationsProvider Provider");
        this.storage.get("pntoken").then((token: string) => {
            this.token = token;
        }).catch((err: any) => {
            this.token = undefined;
        });
    }

    public init(nav: Nav = undefined): void {
        console.log("Initializing push notifications");
        if (nav) {
            this._nav = nav;
        }

        if (localStorage.getItem("notificationsDisabled") == "true") {
            return;
        }

        this.push.createChannel({
            id: "channel1",
            description: "Channel for aplication usage",
            importance: 3
        }).then(() => console.log("Channel created"));

        let push: PushObject = this.push.init({
            android: {
                senderID: "313540665732",
                forceShow: true
            },
            ios: {
                alert: true,
                badge: true,
                sound: false
            },
            windows: {}
        });
        this.pushObject = push;

        push.on("notification").subscribe((n: any) => {
            console.log("Notification");
            this.notificationReceived(<any> n);
        });
        push.on("registration").subscribe((r: any) => {
            console.log("Registration");
            console.log(r);
            this.registrationCompleted(<any> r);
        });
        push.on("error").subscribe((e: any) => {
            console.log("Error");
            this.notificationError(<any> e);
        });
    }

    public register(): void {
        let os_name: string = this.device.model;
        let os_dev: string = this.device.version;
        let platform: string = this.getMobileOperatingSystem();
        let reg_id: string = this.registrationId;
        let user_id: number = this.usersProv.user ? this.usersProv.user.id : undefined;
        console.log(reg_id);
        let dataToSend: any = {token: reg_id, type: platform, model: os_name, os: os_dev, user_id: user_id};
        this.token = reg_id;
        console.log("Sending to web");
        console.log(JSON.stringify(dataToSend));

        this.http.post(ApiUrl + "pushnotifications/register", dataToSend).map((res: Response) => res.json()).subscribe((res: any) => {
            console.log("PN response");
            console.log(res);
        }, (err: Response) => {
            console.log("PN registration error");
            console.log(err);
        });
    }

    public unregister(): void {
        this.pushObject.unregister();
    }

    private notificationReceived(notification: NotificationEventResponse): void {
        console.log("Notification received!");
        console.log(notification);
        NotificationsProvider.availablePost = {
            type: notification.additionalData.post_type,
            id: notification.additionalData.post
        };
        this.ngZone.run(() => {
            if (notification.additionalData.coldstart) {
                this._nav.setRoot(ArticlesPage);
            } else {
                this._nav.setRoot(ArticlesPage);
            }
        });
    }

    private registrationCompleted(registration: RegistrationEventResponse): void {
        console.log(registration.registrationId);
        this.registrationId = registration.registrationId;
        this.register();
    }

    private notificationError(error: Error): void {

    }

    private getMobileOperatingSystem(): string {
        let userAgent: string = navigator.userAgent || navigator.vendor;
        // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent) || /Windows/i.test(userAgent)) {
            return "windows";
        }
        if (/android/i.test(userAgent)) {
            return "android";
        }
        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent)) {
            return "ios";
        }
        return "unknown";
    }

}
