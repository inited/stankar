import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions, Response } from "@angular/http";
import "rxjs/add/operator/map";

import { ApiUrl } from "../app/app.module";
import { UsersProvider } from "./users-provider";

/*
 Generated class for the TermsProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class TermsProvider {

    constructor(public http: Http, private usersProv: UsersProvider) {
        console.log("Hello TermsProvider Provider");
    }

    public getLocations(): Promise<any> {
        return this.http.get(ApiUrl + "terms/get_terms/?taxonomy=event_location&hide_empty=0").map((res: Response) => res.json().terms).toPromise();
    }

    public getOrganizers(): Promise<any> {
        return this.http.get(ApiUrl + "terms/get_terms/?taxonomy=event_organizer&hide_empty=0").map((res: Response) => res.json().terms).toPromise();
    }

    public addOrganizer(name: string, contact: string, exlink: string): Promise<any> {
        let headers: Headers = new Headers();
        headers.append("Content-Type", "application/x-www-form-urlencoded");

        let options: RequestOptions = new RequestOptions({headers: headers});

        return this.http.post(ApiUrl + "terms/create_term/?cookie=" + this.usersProv.cookie,
            "taxonomy=event_organizer" +
            "&term=" + encodeURIComponent(name) +
            "&evcal_org_contact=" + encodeURIComponent(contact) +
            "&evcal_org_exlink=" + encodeURIComponent(exlink) +
            "&_evocal_org_exlink_target=no", options).map((res: Response) => res.json()).toPromise();
    }

    public addLocation(name: string, address: string, description: string, latitude: number, longitude: number): Promise<any> {
        let headers: Headers = new Headers();
        headers.append("Content-Type", "application/x-www-form-urlencoded");

        let options: RequestOptions = new RequestOptions({headers: headers});

        return this.http.post(ApiUrl + "terms/create_term/?cookie=" + this.usersProv.cookie,
            "taxonomy=event_location" +
            "&term=" + encodeURIComponent(name) +
            "&description=" + encodeURIComponent(description) +
            "&location_address=" + encodeURIComponent(address) +
            "&location_lat=" + latitude +
            "&location_lon=" + longitude, options).map((res: Response) => res.json()).toPromise();
    }

    public getEventTypes(): Promise<any> {
        return this.http.get(ApiUrl + "terms/get_terms/?taxonomy=event_type&hide_empty=0").map((res: Response) => res.json().terms).toPromise();
    }

    public getKraje(): Promise<any> {
        return this.http.get(ApiUrl + "terms/get_terms/?taxonomy=event_type_2&hide_empty=0").map((res: Response) => res.json().terms).toPromise();
    }

}
