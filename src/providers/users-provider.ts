import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Storage } from "@ionic/storage";
import { Subject } from "rxjs";
import "rxjs/add/operator/map";

import { ApiUrl } from "../app/app.module";
import { User } from "../model/User";

/*
 Generated class for the UsersProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class UsersProvider {

    public cookie: string;
    public user: User;
    public onLogin: Subject<any>;
    public onLogout: Subject<any>;

    private validated: boolean;

    constructor(public http: Http, public storage: Storage) {
        this.onLogin = new Subject();
        this.onLogout = new Subject();
        console.log("Hello UsersProvider Provider");
    }

    public async init(): Promise<any> {
        try {
            this.cookie = await this.storage.get("cookie");
            this.user = await this.storage.get("user");
            try {
                await this.validateCookie();
                this.user = await this.getUserInfo();
                await this.storage.set("user", this.user);
            } catch (ex) {
                //not validated, continue
            }
            return this.isLoggedIn();
        } catch (ex) {
            this.cookie = undefined;
            this.user = undefined;
        }
    }

    public async login(username: string, password: string): Promise<boolean> {
        try {
            let res: any = await this.http
                .get(ApiUrl + "user/generate_auth_cookie/?insecure=cool&username=" + username + "&password=" + password)
                .map((res: any) => res.json()).toPromise();
            if (res.status == "ok") {
                await this.storage.set("cookie", res.cookie);
                await this.storage.set("user", res.user);
                this.cookie = res.cookie;
                this.user = res.user;
                console.log("Logged in");
                this.validated = true;
                this.onLogin.next();
                return true;
            } else {
                this.onLogout.next();
                console.log("Bad res: ", res);
                throw res;
            }
        } catch (ex) {
            console.log(ex);
            throw ex;
        }
    }

    public async register(user: any): Promise<any> {
        let nonce: string = await this.getNonce();
        return this.http.get(ApiUrl + "user/register/?" +
            "username=" + user.username + "&" +
            "email=" + user.email + "&" +
            "nonce=" + nonce + "&" +
            "first_name=" + user.name + "&" +
            "last_name=" + user.surname + "&" +
            "display_name=" + user.name + " " + user.surname + "&" +
            "user_pass=" + user.password + "&" +
            "insecure=cool").map((res: Response) => res.json()).toPromise();

    }

    public isLoginValidated(): boolean {
        return this.validated;
    }

    public async isLoggedIn(): Promise<boolean> {
        console.log("Checking login");
        if (this.validated) {
            this.onLogin.next();
            return true;
        } else {
            return this.checkLogin();
        }
    }

    public async checkLogin(): Promise<boolean> {
        if (this.cookie && this.user) {
            try {
                let res: any = await this.validateCookie();
                this.validated = true;
                if (res.status == "ok" && res.valid == true) {
                    this.onLogin.next();
                    return true;
                }

                //Nejsem přihlášen
                throw new Error("Not logged in");
            } catch (ex) {
                this.validated = true;
                this.logout();
                this.onLogout.next();
                throw new Error("Not logged in");
            }
        } else {
            this.validated = true;
            this.onLogout.next();
            throw new Error("Not logged in");
        }
    }

    public logout(): void {
        this.storage.remove("cookie");
        this.storage.remove("user");
        this.cookie = undefined;
        this.user = undefined;
        this.onLogout.next();
    }

    private async validateCookie(): Promise<any> {
        console.log("Validating cookie: " + this.cookie);
        return this.http.get(ApiUrl + "user/validate_auth_cookie/?insecure=cool&cookie=" + this.cookie)
            .map((res: Response) => res.json())
            .toPromise();
    }

    private async getUserInfo(): Promise<User> {
        return this.http.get(ApiUrl + "user/get_currentuserinfo/?insecure=cool&cookie=" + this.cookie)
            .map((res: Response) => res.json().user).toPromise();
    }

    private async getNonce(controller: string = "user", method: string = "register"): Promise<string> {
        return this.http.get(ApiUrl + "get_nonce/?controller=" + controller + "&method=" + method).map((res: Response) => res.json().nonce).toPromise();
    }

}
