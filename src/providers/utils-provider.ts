import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";

/*
 Generated class for the UtilsProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class UtilsProvider {

    constructor() {
        console.log("Hello UtilsProvider Provider");
    }

    public isDate(obj: any): boolean {
        return obj != undefined && Object.prototype.toString.call(obj) === "[object Date]";
    }

    public isDatesObject(obj: any): boolean {
        return obj != undefined && obj.beginDate != undefined && obj.endDate != undefined &&
            this.isDate(obj.beginDate) && this.isDate(obj.endDate);
    }

    public parseDate(date: string): Date {
        let dateTime: Array<string> = date.split(" ");
        let dateArr: Array<string> = dateTime[0].split("-");
        let timeArr: Array<string> = dateTime[1].split(":");
        return new Date(parseInt(dateArr[0]), parseInt(dateArr[1]) - 1, parseInt(dateArr[2]), parseInt(timeArr[0]), parseInt(timeArr[1]), parseInt(timeArr[2]), 0);
    }

    public parseHtml(htmlEscaped: string): string  {
        let parser: DOMParser = new DOMParser();
        let dom: Document = parser.parseFromString("<!doctype html><body>" + htmlEscaped, "text/html");
        if (!dom) {
            return htmlEscaped;
        }
        return dom.body.innerHTML;
    }

    public parseText(htmlEscaped: string): string {
        let parser: DOMParser = new DOMParser();
        let dom: Document = parser.parseFromString("<!doctype html><body>" + htmlEscaped, "text/html");
        if (!dom) {
            return htmlEscaped;
        }
        return dom.body.textContent.trim();
    }

}
