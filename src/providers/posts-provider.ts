import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions, Response } from "@angular/http";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { FileUploader } from "ng2-file-upload";
import "rxjs/add/operator/map";
import "rxjs/add/operator/toPromise";

import { ApiUrl } from "../app/app.module";
import { Article } from "../model/Article";
import { Event } from "../model/Event";
import { UserLevel } from "../model/UserLevel";
import { UsersProvider } from "./users-provider";

/*
 Generated class for the PostsProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class PostsProvider {

    constructor(public http: Http, public usersProv: UsersProvider, private transfer: FileTransfer) {
        console.log("Hello PostsProvider Provider");
    }

    public async addArticle(article: Article, item: any, uploader: FileUploader): Promise<any> {
        try {
            let nonce: string = await this.getNonce("create_post");
            article.text = "<div class=\"et_pb_section  et_pb_section_0 et_section_regular\"><div class=\" et_pb_row et_pb_row_0\"><div class=\"et_pb_column et_pb_column_4_4  et_pb_column_0\"><div class=\"et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  et_pb_text_40\">";
            article.text += article.rawText;
            article.text += "</div></div></div></div>";
            let data: any = {
                title: article.rawHeader,
                content: article.text,
                status: "publish",
                categories: article.categories[0]
            };

            let uploadUrl: string = ApiUrl + "create_post/?cookie=" + this.usersProv.cookie + "&nonce=" + nonce;

            uploader.setOptions({
                additionalParameter: data,
                url: uploadUrl
            });

            return new Promise<any>((resolve: any, reject: any): void => {
                //Uploadujeme příspěvek s fotkou
                if (item) {

                    if ((<any> window).cordova) {
                        let fileTransfer: FileTransferObject = this.transfer.create();
                        fileTransfer.upload(item, uploadUrl, {
                            fileKey: "attachment",
                            params: data
                        }).then(resolve).catch(reject);
                    } else {
                        uploader.onCompleteItem = resolve;
                        uploader.onWhenAddingFileFailed = reject;
                        uploader.uploadItem(item);
                    }

                } else {
                    //Uploadujeme příspěvek bez fotky

                    let headers: Headers = new Headers();
                    headers.append("Content-Type", "application/x-www-form-urlencoded");

                    let options: RequestOptions = new RequestOptions({headers: headers});

                    this.http.post(uploadUrl,
                        encodeURI("title=" + article.rawHeader +
                            "&content=" + article.text +
                            "&status=publish&categories=" + article.categories[0]), options).subscribe(resolve, reject);
                }
            });
        } catch (ex) {
            throw new Error("Can't get nonce");
        }
    }

    public async addEvent(event: any, item: any, uploader: FileUploader): Promise<any> {
        try {
            let headers: Headers = new Headers();
            headers.append("Content-Type", "application/x-www-form-urlencoded");

            let options: RequestOptions = new RequestOptions({headers: headers});

            let status: string = "publish";
            if (this.usersProv.user.levels.indexOf(UserLevel.STANKARI) != -1) {
                status = "pending";
            }

            let nonce: string = await this.getNonce("create_post");
            console.log("Got nonce");
            let uploadUrl: string = ApiUrl + "create_post/?cookie=" + this.usersProv.cookie + "&nonce=" + nonce;
            return new Promise<any>((resolve: any, reject: any): void => {
                if (item) {
                    let data: any = this.getEventCustomFieldsObj(event);
                    data.title = event.title;
                    data.content = event.content;
                    data.status = status;
                    data.type = "ajde_events";
                    data.taxonomy_event_location = event.location;
                    data.taxonomy_event_organizer = event.organizer;
                    data.taxonomy_event_type = event.types;
                    data.taxonomy_event_type_2 = event.kraje;

                    if ((<any> window).cordova) {
                        let fileTransfer: FileTransferObject = this.transfer.create();
                        fileTransfer.upload(item, uploadUrl, {
                            fileKey: "attachment",
                            params: data
                        }).then(resolve).catch(reject);
                    } else {
                        uploader.setOptions({
                            additionalParameter: data,
                            url: uploadUrl
                        });
                        uploader.onCompleteItem = resolve;
                        uploader.onWhenAddingFileFailed = reject;
                        uploader.uploadItem(item);
                    }


                } else {
                    let data: string = encodeURI("title=" + event.title + "&" +
                        "content=" + event.content + "&" +
                        "status=" + status + "&" +
                        "type=ajde_events&" +
                        "taxonomy_event_location=" + event.location + "&" +
                        "taxonomy_event_organizer=" + event.organizer + "&" +
                        this.getEventCustomFields(event) +
                        this.getEventTypes(event) +
                        this.getKraje(event));
                    this.http.post(uploadUrl, data, options).subscribe(resolve, reject);
                }
            });
        } catch (ex) {
            throw new Error("Can't get nonce");
        }
    }

    public async deletePost(id: number, postType: string = undefined): Promise<any> {
        try {
            let nonce: string = await this.getNonce("delete_post", "posts");
            return this.http.get(ApiUrl + "posts/delete_post/?cookie=" + this.usersProv.cookie + "&nonce=" + nonce + "&id=" + id + (postType ? "&post_type=" + postType : "")).toPromise();
        } catch (ex) {
            throw new Error("Can't get nonce");
        }
    }

    public async deactivatePost(id: number, postType: string = undefined): Promise<any> {
        try {
            let nonce: string = await this.getNonce("delete_post", "posts");
            return this.http.get(ApiUrl + "posts/deactivate_post/?cookie=" + this.usersProv.cookie + "&nonce=" + nonce + "&id=" + id + (postType ? "&post_type=" + postType : "")).toPromise();
        } catch (ex) {
            throw new Error("Can't get nonce");
        }
    }

    public async getArticles(): Promise<any> {
        let cookie: string = "";
        if (this.usersProv.cookie) {
            cookie = "?cookie=" + this.usersProv.cookie;
        }
        return this.http.get(ApiUrl + "stankar/get_all/" + cookie).map((res: Response) => res.json()).toPromise();
    }

    public async getEvents(count: number = -1): Promise<any> {
        return this.http.get(ApiUrl + "stankar/get_events/?cookie=" + this.usersProv.cookie).map((res: Response) => res.json()).toPromise();
    }

    public async changeWIshlist(action: string, postId: number): Promise<any> {
        return this.http.get(ApiUrl + "stankar/change_wishlist/?cookie=" + this.usersProv.cookie + "&action=" + action + "&event_id=" + postId).map((res: Response) => res.json()).toPromise();
    }

    public async getCategories(): Promise<any> {
        try {
            return this.http.get(ApiUrl + "stankar/get_categories/").map((res: Response) => res.json()).toPromise();
        } catch (err) {
            console.log(err);
        }
    }

    private getEventCustomFields(event: Event): string {
        let eventCustom: string = "custom_fields[evcal_event_color_n]=1&" +
            "custom_fields[evcal_allday]=no&" +
            "custom_fields[tester]=sendOut&" +
            "custom_fields[evcal_event_color]=206177&" +
            "custom_fields[evcal_hide_locname]=no&" +
            "custom_fields[evcal_gmap_gen]=yes&" +
            "custom_fields[evcal_name_over_img]=no&" +
            "custom_fields[evo_access_control_location]=no&" +
            "custom_fields[evcal_repeat]=no&" +
            "custom_fields[evcal_rep_freq]=daily&" +
            "custom_fields[evcal_rep_gap]=1&" +
            "custom_fields[evcal_rep_num]=1&" +
            "custom_fields[evp_repeat_rb]=dom&" +
            "custom_fields[evo_repeat_wom]=1&" +
            "custom_fields[evo_rep_wk]=a:1:{i:0;s:1:\"4\";}&" +
            "custom_fields[evp_repeat_rb_wk]=sing&" +
            "custom_fields[evo_rep_WKwk]=a:1:{i:0;s:1:\"4\";}&" +
            "custom_fields[evcal_lmlink_target]=no&" +
            "custom_fields[evo_hide_endtime]=no&" +
            "custom_fields[evo_span_hidden_end]=no&" +
            "custom_fields[evo_year_long]=no&" +
            "custom_fields[event_year]=" + event.startTime.getFullYear() + "&" +
            "custom_fields[evo_evcrd_field_org]=no&" +
            "custom_fields[evcal_srow]=" + (event.startTime.getTime() / 1000) + "&" +
            "custom_fields[evcal_erow]=" + (event.endTime.getTime() / 1000) + "&" +
            "custom_fields[evoau_disableEditing]=no&" +
            "custom_fields[views]=0";
        console.log(eventCustom);
        return eventCustom;
    }

    private getEventCustomFieldsObj(event: Event): any {
        return {
            "custom_fields[evcal_event_color_n]": 1,
            "custom_fields[evcal_allday]": "no",
            "custom_fields[tester]": "sendOut",
            "custom_fields[evcal_event_color]": 206177,
            "custom_fields[evcal_hide_locname]": "no",
            "custom_fields[evcal_gmap_gen]": "yes",
            "custom_fields[evcal_name_over_img]": "no",
            "custom_fields[evo_access_control_location]": "no",
            "custom_fields[evcal_repeat]": "no",
            "custom_fields[evcal_rep_freq]": "daily",
            "custom_fields[evcal_rep_gap]": 1,
            "custom_fields[evcal_rep_num]": 1,
            "custom_fields[evp_repeat_rb]": "dom",
            "custom_fields[evo_repeat_wom]": 1,
            "custom_fields[evo_rep_wk]": "a:1:{i:0;s:1:\"4\";}",
            "custom_fields[evp_repeat_rb_wk]": "sing",
            "custom_fields[evo_rep_WKwk]": "a:1:{i:0;s:1:\"4\";}",
            "custom_fields[evcal_lmlink_target]": "no",
            "custom_fields[evo_hide_endtime]": "no",
            "custom_fields[evo_span_hidden_end]": "no",
            "custom_fields[evo_year_long]": "no",
            "custom_fields[event_year]": event.startTime.getFullYear(),
            "custom_fields[evo_evcrd_field_org]": "no",
            "custom_fields[evcal_srow]": event.startTime.getTime() / 1000,
            "custom_fields[evcal_erow]": event.endTime.getTime() / 1000,
            "custom_fields[evoau_disableEditing]": "no",
            "custom_fields[views]": 0
        };
    }

    private getEventTypes(event: any): string {
        let returnStr: string = "";
        if (event.types.length > 0) {
            for (let i in event.types) {
                returnStr += "&taxonomy_event_type[" + i + "]=" + event.types[i];
            }
        }

        return returnStr;
    }

    private getKraje(event: any): string {
        let returnStr: string = "";
        if (event.kraje.length > 0) {
            for (let i in event.kraje) {
                returnStr += "&taxonomy_event_type_2[" + i + "]=" + event.kraje[i];
            }
        }

        return returnStr;
    }

    private async getNonce(method: string, controller: string = "posts"): Promise<string> {
        return this.http
            .get(ApiUrl + "get_nonce/?controller=" + controller + "&method=" + method + "&cookie=" + this.usersProv.cookie)
            .map((res: Response) => res.json().nonce).toPromise();
    }

}
