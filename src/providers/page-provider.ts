import {Injectable} from "@angular/core";
import { Http, Response } from "@angular/http";
import {ApiUrl} from "../app/app.module";

@Injectable()
export class PageProvider {

    constructor(public http: Http) {
        console.log("Hello Page Provider");
    }

    public async getNews(): Promise<any> {
        let response: any;
        try {
            response = await this.http.get(ApiUrl + "stankar/get_news/").map((res: Response) => res.json()).toPromise();
            return response;
        } catch (err) {
            return false;
        }
    }
}
