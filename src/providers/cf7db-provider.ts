import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions } from "@angular/http";
import "rxjs/add/operator/map";

import { ApiUrl, BaseUrl } from "../app/app.module";
import { Tip } from "../model/Tip";

/*
 Generated class for the Cf7dbProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class Cf7dbProvider {

    constructor(public http: Http) {
        console.log("Hello Cf7dbProvider Provider");
    }

    public getTips(): Promise<any> {
        return this.http.get(ApiUrl + "cf7db/get/?form_name=Tip na akci").map((res: any) => res.json()).toPromise();
    }

    public createTip(tip: Tip): Promise<any> {
        let data: string = "_wpcf7=81&" +
            "_wpcf7_version=4.7&" +
            "_wpcf7_locale=cs_CZ&" +
            "_wpcf7_unit_tag=wpcf7-f81-p125-o1&" +
            "your-subject=" + tip.subject + "&" +
            "your-message=" + tip.message + "&" +
            "date-225=" + this.parseDate(tip.startDate) + "&" +
            "date-770=" + this.parseDate(tip.endDate) + "&" +
            "url-778=" + tip.web + "&" +
            "_wpcf7_is_ajax_call=1";

        let headers: Headers = new Headers();
        headers.append("Content-Type", "application/x-www-form-urlencoded");

        let options: RequestOptions = new RequestOptions({headers: headers});

        return this.http.post(BaseUrl + "mate-tip-na-akci-vite-o-nejake-podelte-se-s-nami-pridejte-tip-na-akci/", data, options).toPromise();
    }

    private getNonce(): Promise<string> {
        console.log("Getting nonce");
        return this.http.get(BaseUrl + "mate-tip-na-akci-vite-o-nejake-podelte-se-s-nami-pridejte-tip-na-akci/")
            .map((res: any) => res.text()).toPromise().then((res: string) => {
                console.log("Got page");
                let parser: DOMParser = new DOMParser();
                let doc: Document = parser.parseFromString(res, "text/html");
                let nonce: string = doc.getElementsByName("_wpcf7_nonce")[0].getAttribute("value");
                console.log(nonce);
                return nonce;
            });
    }

    private parseDate(date: Date): string {
        return date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear();
    }

}
