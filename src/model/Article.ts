import { SafeHtml } from "@angular/platform-browser";
export interface Article {
    id: number;
    header: string;
    rawHeader: string;
    text: SafeHtml;
    rawText: string;
    date: Date;
    author: string;
    image: string;
    categories: Array<any>;
    favorite: boolean;
    actionInProgress: boolean;
}
