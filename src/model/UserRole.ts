export class UserRole {
    public static SUBSCRIBER: string = "subscriber";
    public static ADMINISTRATOR: string = "administrator";
}
