export interface User {
    id: number;
    username: string;
    nicename: string;
    email: string;
    url: string;
    registered: string;
    displayname: string;
    firstname: string;
    lastname: string;
    nickname: string;
    description: string;
    capabilities: string;
    avatar: any;
    roles: Array<string>;
    levels: Array<string>;
    evo_organizer: string;
}
