export interface Tip {
    subject: string;
    message: string;
    startDate: Date;
    endDate: Date;
    web: string;
}