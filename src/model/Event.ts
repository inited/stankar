import { SafeHtml } from "@angular/platform-browser";
export interface Event {
    id: number;
    header: string;
    rawHeader: string;
    text: SafeHtml;
    rawText: string;
    startTime: Date;
    endTime: Date;
    image: string;
    author: string;
    date: Date;
    allDay: boolean;
    location: string;
    organizer: string;
    organizerId?: number;
    types: Array<any>;
    kraje: Array<any>;
    favorite: boolean;
    actionInProgress: boolean;
}
