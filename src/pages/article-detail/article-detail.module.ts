import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ArticlePopoverComponent } from "../../components/article-popover/article-popover";
import { ArticleDetailPage } from "./article-detail";

@NgModule({
    declarations: [
        ArticleDetailPage
    ],
    imports: [
        IonicPageModule.forChild(ArticleDetailPage),
    ],
    exports: [
        ArticleDetailPage
    ]
})
export class ArticleDetailPageModule {}
