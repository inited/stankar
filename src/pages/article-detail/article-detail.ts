import { Component } from "@angular/core";
import {Events, IonicPage, PopoverController} from "ionic-angular";
import { AlertController, Loading, LoadingController, NavController, NavParams } from "ionic-angular";

import { Article } from "../../model/Article";
import { PostsProvider } from "../../providers/posts-provider";
import { UsersProvider } from "../../providers/users-provider";
import { ArticlePopoverComponent } from "../../components/article-popover/article-popover";

/*
 Generated class for the ArticleDetail page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: "page-article-detail",
    templateUrl: "article-detail.html"
})
export class ArticleDetailPage {

    public article: Article;
    public isLoggedIn: boolean;
    public userRole: string;
    public loggedIn: boolean = false;
    public favorite: number = 0;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                usersProv: UsersProvider,
                private postsProv: PostsProvider,
                private alertCtrl: AlertController,
                private loadingCtrl: LoadingController,
                private popoverCtrl: PopoverController,
                private postsProvider: PostsProvider,
                private events: Events) {
        this.loggedIn = navParams.get("loggedIn");
        console.log(this.loggedIn);
        this.article = navParams.get("article");
        console.log(this.article);
        this.favorite = this.navParams.get("favorite");
        this.isLoggedIn = usersProv.user != undefined;
        if (this.isLoggedIn) {
            this.userRole = usersProv.user.roles[0];
        }
        usersProv.onLogin.subscribe((res: any) => {
            this.loggedIn = true;
        });
        usersProv.onLogout.subscribe((res: any) => {
            this.loggedIn = false;
        });
    }

    public ionViewDidLoad(): void {
        console.log("ionViewDidLoad ArticleDetailPage");
    }

    public showArticlePopover(evt: any): void {
        this.popoverCtrl.create(ArticlePopoverComponent, {
            article: this.article,
            navCtrl: this.navCtrl
        }).present({
            ev: evt
        });
    }

    public changeFavorite(action: string, id: number, article: any): void {
        console.log(article.actionInProgress);
        if (!article.actionInProgress) {
            article.actionInProgress = true;
            if (article.favorite) {
                article.favorite = false;
            } else {
                article.favorite = true;
            }
            this.postsProvider.changeWIshlist(action, id).then((res: any) => {
                console.log(res);
                if (res.status == "ok") {
                    let parsedWishlist: Set<number> = new Set<number>();
                    for (let item2 in res.wishlist) {
                        console.log(item2, res.wishlist[item2]);
                        for (let item in res.wishlist[item2]) {
                            console.log(item, res.wishlist[item2][item]);
                            let splitted: Array <any> = res.wishlist[item2][item].split("-");
                            parsedWishlist.add(parseInt(splitted[0]));
                        }
                    }
                    console.log(res.wishlist, parsedWishlist, parsedWishlist.has(parseInt(article.id)));
                    if (action == "add") {
                        this.favorite = this.favorite + 1;
                    } else {
                        this.favorite = this.favorite - 1;
                    }
                    this.events.publish("favorite:change", this.favorite);
                    this.events.publish("wishlist:set", parsedWishlist);
                    console.log("published articles ", parsedWishlist.size);
                    if (parsedWishlist.has(parseInt(article.id))) {
                        article.favorite = true;
                    } else {
                        article.favorite = false;
                    }
                }
                article.actionInProgress = false;
            }).catch((res: any) => {
                console.log(res);
                article.actionInProgress = false;
            });
        }
    }

}
