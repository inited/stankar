import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { EventTipPage } from "./event-tip";
import {IonDatepickerModule} from "../../components/ion-datepicker/ion-datepicker.module";

@NgModule({
    declarations: [
        EventTipPage,
    ],
    imports: [
        IonicPageModule.forChild(EventTipPage),
        IonDatepickerModule
    ],
    exports: [
        EventTipPage
    ]
})
export class EventTipPageModule {}
