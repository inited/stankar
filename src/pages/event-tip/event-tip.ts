import { Component } from "@angular/core";
import { IonicPage } from "ionic-angular";
import { AlertController, Loading, LoadingController, NavController, NavParams } from "ionic-angular";

import { Tip } from "../../model/Tip";
import { Cf7dbProvider } from "../../providers/cf7db-provider";

/*
 Generated class for the EventTip page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: "page-event-tip",
    templateUrl: "event-tip.html"
})
export class EventTipPage {

    public tip: any = {
        message: "",
        startDate: new Date(),
        subject: "",
        web: "",
        endDate: new Date()
    };
    public dates: {beginDate: Date, endDate: Date} = {
        beginDate: new Date(),
        endDate: new Date()
    };
    public pageType: string = "list";
    public tips: Array<Tip> = [];

    public maxYear: number;
    public networkError: boolean;

    constructor(public navCtrl: NavController, public navParams: NavParams, public cf7dbProv: Cf7dbProvider, public alertCtrl: AlertController, public loadingCtrl: LoadingController) {
        this.getTips();
        this.maxYear = new Date().getFullYear() + 15;
    }

    public ionViewDidLoad(): void {
        console.log("ionViewDidLoad EventTipPage");
    }

    public dateChanged(): void {
        this.tip.startDate = this.dates.beginDate;
        this.tip.endDate = this.dates.endDate;
    }

    public create(): void {

        if (this.tip.message.trim() == "" || !this.tip.startDate || !this.tip.endDate || this.tip.subject.trim() == "") {
            this.alertCtrl.create({
                title: "Chyba",
                message: "Musíte vyplnit všechna povinná pole.",
                buttons: ["OK"]
            }).present();
        } else {
            if (!this.tip.web.startsWith("http") && this.tip.web.trim() != "") {
                this.tip.web = "http://" + this.tip.web;
            }

            if (!this.validURL(this.tip.web)) {
                this.alertCtrl.create({
                    title: "Chyba",
                    message: "Webová stránka neobsahuje platnou webovou adresu.",
                    buttons: ["OK"]
                }).present();
            } else {
                let loading: Loading = this.loadingCtrl.create({
                    content: "Vytvářím tip na akci"
                });
                loading.present();
                this.cf7dbProv.createTip(this.tip).then((res: any) => {
                    loading.dismiss();
                    this.resetTip();
                    this.getTips();
                    this.alertCtrl.create({
                        title: "Hotovo",
                        message: "Tip na akci byl vytvořen a bude zveřejněn na našem webu.",
                        buttons: ["OK"]
                    }).present().then((res: any) => {
                        this.pageType = "list";
                    }).catch((res: any) => {
                    });
                }).catch((res: any) => {
                    loading.dismiss();
                    console.log(res);
                    if (res && (!res.status)) {
                        this.alertCtrl.create({
                            title: "Chyba",
                            message: "Nastala chyba při připojování k internetu, zkontrolujte si prosím vaše spojení.",
                            buttons: ["OK"]
                        }).present();
                    } else {
                        let data: any = res.json();
                        this.alertCtrl.create({
                            title: "Chyba",
                            message: data.error,
                            buttons: ["OK"]
                        }).present();
                    }
                });
            }
        }
    }

    private resetTip(): void {
        this.tip.message = "";
        this.tip.web = "";
        this.tip.subject = "";
        this.tip.endDate = new Date();
        this.tip.startDate = new Date();
    }

    private validURL(str: string): boolean {
        /*let pattern = new RegExp('^(https?:\/\/)?'+ // protocol
         '((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|'+ // domain name
         '((\d{1,3}\.){3}\d{1,3}))'+ // OR ip (v4) address
         '(\:\d+)?(\/[-a-z\d%_.~+]*)*'+ // port and path
         '(\?[;&a-z\d%_.~+=-]*)?'+ // query string
         '(\#[-a-z\d_]*)?$','i'); // fragment locater
         if(!pattern.test(str)) {
         return false;
         } else {
         return true;
         }*/
        return true;
    }

    private getTips(): void {
        this.cf7dbProv.getTips().then((res: any) => {
            console.log(res);
            this.tips = res.values.map((item: any) => {
                return {
                    message: item["your-message"],
                    startDate: item["date-225"],
                    subject: item["your-subject"],
                    web: item["url-778"],
                    endDate: item["date-770"]
                };
            });
        }).catch((res: any) => {
            this.networkError = true;
        });
    }

}
