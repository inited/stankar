import { Component } from "@angular/core";
import {Events, IonicPage, PopoverController} from "ionic-angular";
import { AlertController, Loading, LoadingController, NavController, NavParams } from "ionic-angular";

import { Event } from "../../model/Event";
import { PostsProvider } from "../../providers/posts-provider";
import { TermsProvider } from "../../providers/terms-provider";
import { UsersProvider } from "../../providers/users-provider";
import { ArticlePopoverComponent } from "../../components/article-popover/article-popover";

/*
 Generated class for the EventDetail page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@IonicPage()@Component({
    selector: "page-event-detail",
    templateUrl: "event-detail.html"
})
export class EventDetailPage {

    public event: Event;
    public organizer: any;
    public isLoggedIn: boolean;
    public userRole: string;
    public loggedIn: boolean;
    public favorite: number = 0;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public termsProv: TermsProvider,
                private alertCtrl: AlertController,
                private loadingCtrl: LoadingController,
                private postsProv: PostsProvider,
                usersProv: UsersProvider,
                private popoverCtrl: PopoverController,
                private events: Events) {
        this.loggedIn = navParams.get("loggedIn");
        console.log(this.loggedIn);
        this.event = navParams.get("event");
        console.log("Event:", this.event);
        this.favorite = this.navParams.get("favorite");
        termsProv.getOrganizers().then((terms: any) => {
            this.organizer = terms.find((term: any) => term.term_id == this.event.organizerId);
            console.log(this.organizer);
        });
        this.isLoggedIn = usersProv.user != undefined;
        if (this.isLoggedIn) {
            this.userRole = usersProv.user.roles[0];
        }

        usersProv.onLogin.subscribe((res: any) => {
            this.loggedIn = true;
        });
        usersProv.onLogout.subscribe((res: any) => {
            this.loggedIn = false;
        });
    }

    public ionViewDidLoad(): void {
        console.log("ionViewDidLoad EventDetailPage");
    }

    public showArticlePopover(evt: any): void {
        this.popoverCtrl.create(ArticlePopoverComponent, {
            article: this.event,
            navCtrl: this.navCtrl,
            postType: "ajde_events"
        }).present({
            ev: evt
        });
    }

    public isSameDay(event: Event): boolean
    {
        return event.startTime.getUTCDate() == event.endTime.getUTCDate() &&
            event.startTime.getUTCMonth() == event.endTime.getUTCMonth() &&
            event.startTime.getUTCFullYear() == event.endTime.getUTCFullYear();
    }

    public getUTCLocalizedDate(date: Date): string
    {
        return date.getUTCDate() + "." +
            (date.getUTCMonth() + 1) + "." +
            date.getUTCFullYear();
    }

    public changeFavorite(action: string, id: number, article: any): void {
        console.log(article.actionInProgress);
        if (!article.actionInProgress) {
            article.actionInProgress = true;
            if (article.favorite) {
                article.favorite = false;
            } else {
                article.favorite = true;
            }
            this.postsProv.changeWIshlist(action, id).then((res: any) => {
                console.log(res);
                if (res.status == "ok") {
                    let parsedWishlist: Set<number> = new Set<number>();
                    for (let item2 in res.wishlist) {
                        console.log(item2, res.wishlist[item2]);
                        for (let item in res.wishlist[item2]) {
                            console.log(item, res.wishlist[item2][item]);
                            let splitted: Array <any> = res.wishlist[item2][item].split("-");
                            parsedWishlist.add(parseInt(splitted[0]));
                        }
                    }
                    console.log(res.wishlist, parsedWishlist, parsedWishlist.has(parseInt(article.id)));
                    if (action == "add") {
                        this.favorite = this.favorite + 1;
                    } else {
                        this.favorite = this.favorite - 1;
                    }
                    this.events.publish("favorite:change", this.favorite);
                    this.events.publish("wishlist:set", parsedWishlist);
                    console.log("published articles ", parsedWishlist.size);
                    if (parsedWishlist.has(parseInt(article.id))) {
                        article.favorite = true;
                    } else {
                        article.favorite = false;
                    }
                }
                article.actionInProgress = false;
            }).catch((res: any) => {
                console.log(res);
                article.actionInProgress = false;
            });
        }
    }


}
