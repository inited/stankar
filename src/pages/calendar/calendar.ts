import { Component } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { IonicPage } from "ionic-angular";
import { NavController, NavParams } from "ionic-angular";

import { Event } from "../../model/Event";
import { User } from "../../model/User";
import { NotificationsProvider } from "../../providers/notifications-provider";
import { PostsProvider } from "../../providers/posts-provider";
import { TermsProvider } from "../../providers/terms-provider";
import { UsersProvider } from "../../providers/users-provider";
import { UtilsProvider } from "../../providers/utils-provider";

/*
 Generated class for the Calendar page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: "page-calendar",
    templateUrl: "calendar.html"
})
export class CalendarPage {

    public calendarType: string = "calendar";
    public loadingData: boolean = true;
    public today: Date = new Date();
    public currentDate: Date = this.today;
    public filterDate: Date;
    public filteredEventTypes: Array<number> = [];
    public eventSource: Array<Event> = [];
    public filteredEvents: Array<Event> = [];
    public eventTypes: Array<any> = [];
    public kraje: Array<any> = [];
    public filteredKraje: Array<number> = [];
    public poradatele: Array<any> = [];
    public filteredPoradatele: Array<number> = [];
    public networkError: boolean;

    constructor(public navCtrl: NavController, public navParams: NavParams, public usersProv: UsersProvider, public postsProv: PostsProvider, private sanitizer: DomSanitizer, public termsProv: TermsProvider, public utilsProv: UtilsProvider) {
        this.postsProv.getEvents().then((res: any) => {
            this.eventSource = res.events.filter((i: any) => i.custom_fields.evcal_srow).map((item: any) => {
                return {
                    id: item.id,
                    rawHeader: this.utilsProv.parseText(item.title),
                    header: this.utilsProv.parseHtml(item.title),
                    rawText: this.utilsProv.parseText(item.content),
                    text: this.sanitizer.bypassSecurityTrustHtml(this.utilsProv.parseHtml(item.content)),
                    startTime: item.custom_fields.evcal_srow ? new Date(item.custom_fields.evcal_srow * 1000) : new Date(0),
                    endTime: item.custom_fields.evcal_erow ? new Date(item.custom_fields.evcal_erow * 1000) : new Date(0),
                    image: item.thumbnail_images && item.thumbnail_images.full ? item.thumbnail_images.full.url : undefined,
                    allDay: item.custom_fields.evcal_allday == "yes",
                    types: item.taxonomy_event_type,
                    kraje: item.taxonomy_event_type_2,
                    author: item.author.nickname,
                    date: this.utilsProv.parseDate(item.date),
                    location: item.taxonomy_event_location[0] ? item.taxonomy_event_location[0].title : "",
                    organizer: item.taxonomy_event_organizer[0] ? item.taxonomy_event_organizer[0].title : "",
                    organizerId: item.taxonomy_event_organizer[0] ? item.taxonomy_event_organizer[0].id : 0
                };
            }).filter((item: Event) => item.startTime.getTime() > ((new Date()).getTime() - 86400000))
                .sort((item1: Event, item2: Event) => item1.startTime.getTime() - item2.startTime.getTime());
            this.loadingData = false;
            console.log(this.eventSource);
            this.filter();
            if (NotificationsProvider.availablePost && NotificationsProvider.availablePost.type == "event") {
                let event: Event = this.eventSource.find((event: Event) => event.id == NotificationsProvider.availablePost.id);
                if (event) {
                    this.eventSelected(event);
                }
            }
        }).catch((res: any) => {
            this.networkError = true;
        });

        this.getTerms();
    }

    public ionViewDidLoad(): void {
        console.log("ionViewDidLoad CalendarPage");
    }

    public currentDateChanged(date: Date): void {
        this.currentDate = date;
        console.log(date);
    }

    public createNew(): void {
        this.navCtrl.setRoot("NewEventPage");
    }

    public filter(): void {
        if (!(this.filterDate instanceof Date) && this.filterDate != undefined) {
            this.filterDate = new Date(this.filterDate);
        }
        console.log(this.eventSource);
        this.filteredEvents = this.eventSource.filter((item: Event) => {
            if (this.filterDate) {
                if (this.isHigher(item.startTime, this.filterDate) || this.isLower(item.endTime, this.filterDate)) {
                    return false;
                }
            }
            if (this.filteredEventTypes.length > 0) {
                if (this.filteredEventTypes.filter((type: number) => item.types.map((type: any) => type.id).indexOf(type) > -1).length == 0) {
                    return false;
                }
            }
            if (this.filteredKraje.length > 0) {
                if (this.filteredKraje.filter((kraj: number) => item.kraje.map((kraj: any) => kraj.id).indexOf(kraj) > -1).length == 0) {
                    return false;
                }
            }
            if (this.filteredPoradatele.length > 0) {
                if (this.filteredPoradatele.indexOf(item.organizerId) == -1) {
                    return false;
                }
            }
            return true;
        });
        console.log(this.filteredEvents);
    }

    public eventSelected(event: Event): void {
        this.navCtrl.push("EventDetailPage", {event: event});
    }

    public clearDate(): void {
        console.log("Clearing date");
        this.filterDate = undefined;
        this.filter();
    }

    public isSameDay(event: Event): boolean
    {
        return event.startTime.getUTCDate() == event.endTime.getUTCDate() &&
            event.startTime.getUTCMonth() == event.endTime.getUTCMonth() &&
            event.startTime.getUTCFullYear() == event.endTime.getUTCFullYear();
    }

    public getUTCLocalizedDate(date: Date): string
    {
        return date.getUTCDate() + "." +
            (date.getUTCMonth() + 1) + "." +
            date.getUTCFullYear();
    }

    private async getTerms(): Promise<any> {
        try {
            this.eventTypes = await this.termsProv.getEventTypes();
            this.kraje = await this.termsProv.getKraje();
            this.poradatele = await this.termsProv.getOrganizers();
        } catch (ex) {

        }
    }

    private isLower(firstDate: Date, secondDate: Date): boolean {
        if (firstDate.getFullYear() > secondDate.getFullYear()) {
            return false;
        } else if (firstDate.getFullYear() < secondDate.getFullYear()) {
            return true;
        } else {
            if (firstDate.getMonth() > secondDate.getMonth()) {
                return false;
            } else if (firstDate.getMonth() < secondDate.getMonth()) {
                return true;
            } else {
                return firstDate.getDate() < secondDate.getDate();
            }
        }
    }

    private isHigher(firstDate: Date, secondDate: Date): boolean {
        if (firstDate.getFullYear() < secondDate.getFullYear()) {
            return false;
        } else if (firstDate.getFullYear() > secondDate.getFullYear()) {
            return true;
        } else {
            if (firstDate.getMonth() < secondDate.getMonth()) {
                return false;
            } else if (firstDate.getMonth() > secondDate.getMonth()) {
                return true;
            } else {
                return firstDate.getDate() > secondDate.getDate();
            }
        }
    }

}
