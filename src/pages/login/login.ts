import { Component } from "@angular/core";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { IonicPage } from "ionic-angular";
import { AlertController, Loading, LoadingController, NavController, NavParams } from "ionic-angular";
import { BaseUrl } from "../../app/app.module";

import { NotificationsProvider } from "../../providers/notifications-provider";
import { UsersProvider } from "../../providers/users-provider";

/*
 Generated class for the Login page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: "page-login",
    templateUrl: "login.html"
})
export class LoginPage {

    public username: string = "";
    public password: string = "";

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public usersProv: UsersProvider,
                public alertCtrl: AlertController,
                public loadingCtrl: LoadingController,
                public notificationsProv: NotificationsProvider,
                public inAppBrowser: InAppBrowser) {
    }

    public ionViewDidLoad(): void {
        console.log("ionViewDidLoad LoginPage");
    }

    public login(): void {
        if (this.username.trim() == "" && this.password.trim() == "") {
            this.alertCtrl.create({
                title: "Chyba",
                message: "Zadejte prosím vaše jméno a heslo",
                buttons: ["OK"]
            }).present();
            return;
        }
        let loading: Loading = this.loadingCtrl.create({
            content: "Probíhá přihlašování"
        });
        loading.present();
        this.usersProv.login(this.username, this.password).then((res: any) => {
            this.notificationsProv.register();
            loading.dismiss();
            this.navCtrl.setRoot("ArticlesPage");
        }).catch((err: any) => {
            console.log(err);
            loading.dismiss();
            if (err && (!err.status || (err.status && err.status != "error"))) {
                this.alertCtrl.create({
                    title: "Chyba",
                    message: "Nastala chyba při připojování k internetu, zkontrolujte si prosím vaše spojení.",
                    buttons: ["OK"]
                }).present();
            } else {
                this.alertCtrl.create({
                    title: "Chyba",
                    message: "Špatně zadané jméno nebo heslo",
                    buttons: ["OK"]
                }).present();
            }
        });
    }

    public goToRegistration(): void {
        this.navCtrl.push("RegistrationPage");
    }

    public forgottenPassword(): void {
        this.inAppBrowser.create(BaseUrl + "stankar-cz-obnova-reset-hesla/", "_system");
    }

}
