import { Component } from "@angular/core";
import { IonicPage } from "ionic-angular";
import {PageProvider} from "../../providers/page-provider";

/*
 Generated class for the NewEvent page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@IonicPage()@Component({
    selector: "page-news",
    templateUrl: "news.html"
})
export class NewsPage {

    public page: any;
    public content: string;

    constructor(public pageProv: PageProvider) {
        this.getContent();
    }

    public ionViewDidLoad(): void {
        console.log("ionViewDidLoad NewsPage");
    }

    public async getContent(): Promise<any> {
        let response: any = await this.pageProv.getNews();
        console.log("Response :", response)
        this.content = response.content;
        console.log("Content: ", this.content)
    }

}
