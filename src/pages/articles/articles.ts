import { Component, NgZone } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { SplashScreen } from "@ionic-native/splash-screen";
import { Events, IonicPage, NavController, NavParams } from "ionic-angular";
import { Subscription } from "rxjs/Subscription";

import { Article } from "../../model/Article";
import { Event } from "../../model/Event";
import { NotificationsProvider } from "../../providers/notifications-provider";
import { PostsProvider } from "../../providers/posts-provider";
import { UsersProvider } from "../../providers/users-provider";
import { UtilsProvider } from "../../providers/utils-provider";

/*
 Generated class for the Articles page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: "page-articles",
    templateUrl: "articles.html"
})
export class ArticlesPage {

    public articles: Array<any> = [];
    public shownArticles: Array<Article | Event> = [];
    public selectedPage: number = 0;
    public pageCount: number;
    public loginSubs: Subscription;
    public logoutSubs: Subscription;
    public networkError: boolean;
    public categoriesWithColors: Array<any>;
    public isLoggedIn: boolean = false;
    public favorite: number = 0;
    public colors: Array<string> = ["Blue", "BlueViolet", "Brown", "BurlyWood", "CadetBlue", "Chartreuse", "CornflowerBlue", "Crimson", "Cyan", "DarkBlue", "DarkGoldenRod", "DarkGray", "DarkGrey", "DarkGreen", "DarkKhaki", "DarkMagenta", "DarkOliveGreen", "Darkorange", "DarkOrchid", "DarkRed", "DarkSalmon", "DarkSeaGreen", "DarkSlateBlue", "DarkSlateGray", "DarkSlateGrey", "DarkTurquoise", "DarkViolet", "DeepPink", "DeepSkyBlue", "DimGray", "DimGrey", "DodgerBlue", "FireBrick", "FloralWhite", "ForestGreen", "Fuchsia", "Gainsboro", "GhostWhite", "Gold", "GoldenRod", "Gray", "Grey", "Green", "GreenYellow", "HoneyDew", "HotPink", "IndianRed", "Indigo", "Ivory", "Khaki", "Lavender", "LavenderBlush", "LawnGreen", "LemonChiffon", "LightBlue", "LightCoral", "LightCyan", "LightGoldenRodYellow", "LightGray", "LightGrey", "LightGreen", "LightPink", "LightSalmon", "LightSeaGreen", "LightSkyBlue", "LightSlateGray", "LightSlateGrey", "LightSteelBlue", "LightYellow", "Lime", "LimeGreen", "Linen", "Magenta", "Maroon", "MediumAquaMarine", "MediumBlue", "MediumOrchid", "MediumPurple", "MediumSeaGreen", "MediumSlateBlue", "MediumSpringGreen", "MediumTurquoise", "MediumVioletRed", "MidnightBlue", "MintCream", "MistyRose", "Moccasin", "NavajoWhite", "Navy", "OldLace", "Olive", "OliveDrab", "Orange", "OrangeRed", "Orchid", "PaleGoldenRod", "PaleGreen", "PaleTurquoise", "PaleVioletRed", "PapayaWhip", "PeachPuff", "Peru", "Pink", "Plum", "PowderBlue", "Purple", "Red", "RosyBrown", "RoyalBlue", "SaddleBrown", "Salmon", "SandyBrown", "SeaGreen", "SeaShell", "Sienna", "Silver", "SkyBlue", "SlateBlue", "SlateGray", "SlateGrey", "Snow", "SpringGreen", "SteelBlue", "Tan", "Teal", "Thistle", "Tomato", "Turquoise", "Violet", "Wheat", "Yellow", "YellowGreen"];


    constructor(public navCtrl: NavController, public navParams: NavParams, public postsProvider: PostsProvider,
                public sanitizer: DomSanitizer, public usersProv: UsersProvider, public splashScreen: SplashScreen,
                private ngZone: NgZone, private utilsProv: UtilsProvider, private events: Events) {

        if (this.usersProv.isLoginValidated()) {
            this.isLoggedIn = true;
            console.log("Validated");
            this.usersProv.checkLogin().then(() => {
                this.isLoggedIn = true;
                this.loginChecked();
            }).catch(() => {
                this.isLoggedIn = false;
                this.loginChecked();
            });
        } else {
            this.isLoggedIn = false;
            console.log("Not validated");
            this.loginSubs = usersProv.onLogin.subscribe((res: any) => {
                this.isLoggedIn = true;
                this.loginChecked();
            });
            this.logoutSubs = usersProv.onLogout.subscribe((res: any) => {
                this.isLoggedIn = false;
                this.loginChecked();
            });
        }

        this.events.subscribe("wishlist:set", wishlist => {
            for (let article of this.shownArticles) {
                if (wishlist.has(article.id)) {
                    article.favorite = true;
                } else {
                    article.favorite = false;
                }
            }
        });

    }

    public async loginChecked(): Promise<any> {
        /*
        this.categoriesWithColors = [];
        let categories: any = await this.postsProvider.getCategories();
        console.log("categories", categories);
        try {
            for (let cat in categories.categories) {
                let newCat: any = {
                    name: categories.categories[cat],
                    color: this.colors[cat]
                };
                this.categoriesWithColors.push(newCat);
            }
        } catch (err) {}
        console.log("cat with col", this.categoriesWithColors);
        */
        try {
            let response: any = await this.postsProvider.getArticles();
            this.articlesDownloaded(response.posts, response.events, response.wishlist);
        }
        catch (ex) {
            this.networkError = true;
            this.splashScreen.hide();
        }
    }

    public ionViewDidLoad(): void {
        console.log("ionViewDidLoad ArticlesPage");
    }

    public showDetail(article: Article | Event): void {
        if ((<any> article).kraje) {
            this.navCtrl.push("EventDetailPage", {event: article, loggedIn: this.isLoggedIn, favorite: this.favorite});
        } else {
            this.navCtrl.push("ArticleDetailPage", {article: article, loggedIn: this.isLoggedIn, favorite: this.favorite});
        }
    }

    public selectPage(pageNumber: number): void {
        console.log("Selected page: " + pageNumber);
        this.selectedPage = pageNumber;
        this.shownArticles = this.articles.filter((value: Article|Event, index: number, array: Array<Article|Event>) => {
            return !(index < this.selectedPage * 10 || index > this.selectedPage * 10 + 10);
        });
        console.log(this.shownArticles);
    }

    public changeFavorite(action: string, id: number, article: any): void {
        console.log(article.actionInProgress);
        if (!article.actionInProgress) {
            article.actionInProgress = true;
            if (article.favorite) {
                article.favorite = false;
            } else {
                article.favorite = true;
            }
            this.postsProvider.changeWIshlist(action, id).then((res: any) => {
                console.log(res);
                if (res.status == "ok") {
                    let parsedWishlist: Set<number> = new Set<number>();
                    for (let item2 in res.wishlist) {
                        console.log(item2, res.wishlist[item2]);
                        for (let item in res.wishlist[item2]) {
                            console.log(item, res.wishlist[item2][item]);
                            let splitted: Array <any> = res.wishlist[item2][item].split("-");
                            parsedWishlist.add(parseInt(splitted[0]));
                        }
                    }
                    console.log(res.wishlist, parsedWishlist, parsedWishlist.has(parseInt(article.id)));
                    if (action == "add") {
                        this.favorite = this.favorite + 1;
                    } else {
                        this.favorite = this.favorite - 1;
                    }
                    this.events.publish("favorite:change", this.favorite);
                    console.log("published articles ", parsedWishlist.size);
                    if (parsedWishlist.has(parseInt(article.id))) {
                        article.favorite = true;
                    } else {
                        article.favorite = false;
                    }
                }
                article.actionInProgress = false;
            }).catch((res: any) => {
                console.log(res);
                article.actionInProgress = false;
            });
        }
    }

    private articlesDownloaded(articles: Array<any>, events: Array<any>, wishlist: Array<any>): void {
        let parsedWishlist: Set<number> = new Set<number>();
        for (let i in wishlist) {
            let splitted: Array <any> = wishlist[i].split("-");
            parsedWishlist.add(parseInt(splitted[0]));
        }
        let favoriteLet: Set<number> = new Set<number>();
        //this.events.publish("favorite:change", parsedWishlist.size);
        console.log("wished size ", parsedWishlist.size);
        this.articles.length = 0;
        this.articles = articles.map((item: any) => {
            let fav: boolean = false;
            if (parsedWishlist.has(parseInt(item.id))) {
                fav = true;
            }
            return {
                id: item.id,
                header: this.utilsProv.parseHtml(item.title),
                rawHeader: this.utilsProv.parseText(item.title),
                text: this.sanitizer.bypassSecurityTrustHtml(this.utilsProv.parseHtml(item.content)),
                rawText: this.utilsProv.parseText(item.content),
                date: this.utilsProv.parseDate(item.date),
                author: item.author.nickname,
                image: this.getArticleImage(item),
                categories: item.categories,
                favorite: fav,
                actionInProgress: false
            };
        });
        if (this.usersProv.user) {
            events.map((item: any) => {
                let fav: boolean = false;
                if (parsedWishlist.has(parseInt(item.id))) {
                    fav = true;
                }
                return {
                    id: item.id,
                    rawHeader: this.utilsProv.parseText(item.title),
                    header: this.utilsProv.parseHtml(item.title),
                    rawText: this.utilsProv.parseText(item.content),
                    text: this.sanitizer.bypassSecurityTrustHtml(this.utilsProv.parseHtml(item.content)),
                    startTime: item.custom_fields.evcal_srow ? new Date(item.custom_fields.evcal_srow * 1000) : new Date(0),
                    endTime: item.custom_fields.evcal_erow ? new Date(item.custom_fields.evcal_erow * 1000) : new Date(0),
                    image: item.thumbnail_images && item.thumbnail_images.full ? item.thumbnail_images.full.url : undefined,
                    allDay: item.custom_fields.evcal_allday == "yes",
                    types: item.taxonomy_event_type,
                    kraje: item.taxonomy_event_type_2,
                    author: item.author.nickname,
                    date: this.utilsProv.parseDate(item.date),
                    location: item.taxonomy_event_location[0] ? item.taxonomy_event_location[0].title : "",
                    organizer: item.taxonomy_event_organizer[0] ? item.taxonomy_event_organizer[0].title : "",
                    organizerId: item.taxonomy_event_organizer[0] ? item.taxonomy_event_organizer[0].id : 0,
                    color: "darkcyan",
                    favorite: fav,
                    actionInProgress: false
                };
            }).filter((item: Event) => item.startTime.getTime() > ((new Date()).getTime() - 86400000)).map((item: Event) => {
                this.articles.push(item);
            });
        }
        this.articles = this.articles.sort((item1: Article|Event, item2: Article|Event) => {
            return item2.id - item1.id;
        });

        this.articles.filter(value => parsedWishlist.has(value.id)).map((item: Event|Article) => {
            favoriteLet.add(item.id);
        });

        console.log("Favorite: ", favoriteLet);
        this.favorite = favoriteLet.size;
        this.events.publish("favorite:change", this.favorite);

        console.log(this.articles);
        for (let article of this.articles) {
            let first: boolean = true;
            try {
                for (let cat of article.categories) {
                    console.log("slug", cat.slug, cat);
                    if (cat.slug == "clanky") {
                        article.color = "darkblue";
                    } else if (cat.slug == "upozorneni-poradatele") {
                        article.color = "#83502e";
                    } else {
                        article.color = "darkgreen";
                    }
                }
            } catch (err) {}
        }
        let pageCount: number = Math.ceil(this.articles.length / 10);
        if (pageCount == 0) {
            pageCount = 1;
        }
        this.pageCount = pageCount;
        this.selectPage(0);
        this.splashScreen.hide();
        if (this.loginSubs) {
            this.loginSubs.unsubscribe();
            this.logoutSubs.unsubscribe();
        }
        setTimeout(() => {
            this.ngZone.run(() => {
                if (NotificationsProvider.availablePost) {
                    let article: Article|Event = this.articles.find((article: Article|Event) => article.id == NotificationsProvider.availablePost.id);
                    if (article) {
                        this.showDetail(article);
                        NotificationsProvider.availablePost = undefined;
                    }
                }
            });
        }, 300);

    }

    private getArticleImage(article: any): string {
        if (article.thumbnail && article.thumbnail.trim() != "") {
            return article.thumbnail;
        }
        return article.attachments[0] ? article.attachments[0].url : undefined;
    }

}
