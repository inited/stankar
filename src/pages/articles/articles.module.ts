import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { TruncatePipe } from "../../pipes/truncate";
import { ArticlesPage } from "./articles";

@NgModule({
    declarations: [
        ArticlesPage,
        //Pipes
        TruncatePipe
    ],
    imports: [
        IonicPageModule.forChild(ArticlesPage),
    ],
    exports: [
        ArticlesPage
    ]
})
export class ArticlesPageModule {}
