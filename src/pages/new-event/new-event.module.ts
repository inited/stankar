import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { FileUploadModule } from "ng2-file-upload";
import { IonDatepickerModule } from "../../components/ion-datepicker/ion-datepicker.module";
import { NewEventPage } from "./new-event";

@NgModule({
    declarations: [
        NewEventPage,
    ],
    imports: [
        FileUploadModule,
        IonicPageModule.forChild(NewEventPage),
        IonDatepickerModule
    ],
    exports: [
        NewEventPage
    ]
})
export class NewEventPageModule {}
