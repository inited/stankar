import { Component } from "@angular/core";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { ImagePicker } from "@ionic-native/image-picker";
import { IonicPage } from "ionic-angular";
import {
    AlertController, Loading, LoadingController, Modal, ModalController, NavController,
    NavParams
} from "ionic-angular";
import { FileUploader } from "ng2-file-upload";

import { Storage } from "@ionic/storage";
import { UserLevel } from "../../model/UserLevel";
import { PostsProvider } from "../../providers/posts-provider";
import { TermsProvider } from "../../providers/terms-provider";
import { UsersProvider } from "../../providers/users-provider";
import { User } from "../../model/User";
import { UserRole } from "../../model/UserRole";

/*
 Generated class for the NewEvent page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@IonicPage()@Component({
    selector: "page-new-event",
    templateUrl: "new-event.html"
})
export class NewEventPage {

    public event: any = {
        endTime: new Date(),
        startTime: new Date(),
        content: "",
        title: "",
        allDay: false,
        image: "",
        location: undefined,
        organizer: undefined,
        types: [],
        kraje: [undefined]
    };
    public organizerName: string;
    public locations: Array<any> = [];
    public eventTypes: Array<any> = [];
    public kraje: Array<any> = [];
    public organizers: Array<any> = [];

    public previewUrl: SafeUrl;
    public uploader: FileUploader = new FileUploader({url: ""});
    public selectedFile: any = undefined;

    public maxYear: number;
    public networkError: boolean;
    public user: User;


    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public alertCtrl: AlertController,
                public loadingCtrl: LoadingController,
                public postsProv: PostsProvider,
                public termsProv: TermsProvider,
                public usersProv: UsersProvider,
                private sanitizer: DomSanitizer,
                private imagePicker: ImagePicker,
                private modalCtrl: ModalController,
                private storage: Storage) {
        this.user = this.usersProv.user;
        this.termsProv.getLocations().then(async (terms: any) => {
            this.locations = terms;
            const location: string = await this.storage.get("eventLocation");
            if (location && this.locations.filter((l: any) => l.slug == location).length > 0) {
                this.event.location = location;
            }
        }, (err: any) => {
            this.networkError = true;
        });
        this.termsProv.getEventTypes().then((terms: any) => {
            this.eventTypes = terms;
        }, (err: any) => {
            this.networkError = true;
        });
        this.termsProv.getKraje().then(async (terms: any) => {
            this.kraje = terms;
            const kraj: string = await this.storage.get("eventKraj");
            if (kraj && this.kraje.filter((k: any) => k.term_id == kraj).length > 0) {
                this.event.kraje[0] = kraj;
            }
        }, (err: any) => {
            this.networkError = true;
        });
        this.termsProv.getOrganizers().then(async (terms: any) => {
            this.organizers = terms;
            console.log(terms);
            if (this.user.roles[0] == UserRole.ADMINISTRATOR) {
                const organizer: string = await this.storage.get("eventOrganizer");
                if (organizer && this.organizers.filter((o: any) => o.slug == organizer).length > 0) {
                    this.event.organizer = organizer;
                }
            } else {
                const term: any = this.organizers.filter((o: any) => o.term_id == this.user.evo_organizer)[0];
                this.organizerName = term.name;
                this.event.organizer = term.slug;
            }
        }, (err: any) => {
            this.networkError = true;
        });
        this.previewUrl = sanitizer.bypassSecurityTrustUrl("assets/no-image.jpg");
        let that: NewEventPage = this;
        this.uploader.onAfterAddingFile = (fileItem: any): void => {
            that.fileSelected(fileItem);
        };
        this.maxYear = new Date().getFullYear() + 15;
    }

    public ionViewDidLoad(): void {
        console.log("ionViewDidLoad NewEventPage");
    }

    public startTimeChanged(): void {
        if (this.event.startTime > this.event.endTime) {
            this.event.endTime = this.event.startTime;
        }
    }

    public endTimeChanged(): void {
        if (this.event.startTime > this.event.endTime) {
            this.event.startTime = this.event.endTime;
        }
    }

    public create(): void {
        if (this.event.title.trim() == "" ||
            this.event.content.trim() == "" ||
            this.event.location == undefined ||
            typeof(this.event.location) !== "string" ||
            this.event.organizer == undefined ||
            typeof(this.event.organizer) !== "string" ||
            this.event.kraje.filter((kraj: any) => kraj != undefined && typeof(kraj) === "number").length == 0 ||
            !Array.isArray(this.event.types) ||
            this.event.types.filter((type: any) => type != undefined && typeof(type) === "number").length == 0) {
            this.alertCtrl.create({
                title: "Chyba",
                message: "Vyplňte prosím informace o akci",
                buttons: ["OK"]
            }).present();
            return;
        }
        if (this.event.startTime.getTime() > this.event.endTime.getTime()) {
            this.alertCtrl.create({
                title: "Chyba",
                message: "Začátek akce musí být před jejím koncem",
                buttons: ["OK"]
            }).present();
            return;
        }

        let loading: Loading = this.loadingCtrl.create({
            content: "Vytvářím akci"
        });
        loading.present();
        this.postsProv.addEvent(this.event, this.selectedFile, this.uploader).then(async (res: any) => {
            loading.dismiss();
            await this.saveAutoFills();
            if (this.usersProv.user.levels.indexOf(UserLevel.STANKARI) != -1) {
                this.resetEvent();
                this.alertCtrl.create({
                    title: "Hotovo",
                    message: "Vaše akce byla úspěšně vytvořena a po schválení administrátorem bude přidána do kalendáře.",
                    buttons: ["OK"]
                }).present();
            } else {
                this.navCtrl.setRoot("CalendarPage");
            }
        }).catch((res: any) => {
            loading.dismiss();
            if (res.status == 403) {
                this.alertCtrl.create({
                    title: "Chyba",
                    message: "Nemáte právo přidávat nové akce, kontaktujte prosím administrátora webu.",
                    buttons: ["OK"]
                }).present();
            } else if (!res.status) {
                this.alertCtrl.create({
                    title: "Chyba",
                    message: "Nastala chyba při připojování k internetu, zkontrolujte si prosím vaše spojení.",
                    buttons: ["OK"]
                }).present();
            } else {
                let data: any = res.json();
                let error: string = data.error;
                if (data.error == "Not logged in") {
                    error = "Nejste přihlášen.";
                    this.usersProv.logout();
                    this.navCtrl.setRoot("ArticlesPage");
                }
                this.alertCtrl.create({
                    title: "Chyba",
                    message: error,
                    buttons: ["OK"]
                }).present();
            }
        });
        console.log("Creating");
        console.log(this.event);
    }

    public showFileUpload(input: HTMLInputElement): void {
        this.imagePicker.getPictures({
            maximumImagesCount: 1,
            width: 1024,
            height: 1024
        }).then((results: Array<any>) => {
            if (!results[0] || results[0] == "") {
                this.previewUrl = this.sanitizer.bypassSecurityTrustUrl("assets/no-image.jpg");
                this.selectedFile = undefined;
            } else {
                this.previewUrl = this.sanitizer.bypassSecurityTrustUrl(results[0]);
                this.selectedFile = results[0];
            }
            console.log(results);
        }).catch((res: any) => {
            input.click();
        });
    }

    public fileSelected(fileItem: any): void {
        this.selectedFile = fileItem;
        this.selectedFile.alias = "attachment";
        console.log(fileItem);
        this.previewUrl = this.sanitizer.bypassSecurityTrustUrl((window.URL.createObjectURL(fileItem._file)));
    }

    public newLocation(): void {
        let modal: Modal = this.modalCtrl.create("AddLocationPage");
        modal.onDidDismiss((res: any) => {
            console.log("Created:", res);
            if (res) {
                this.locations.push(res);
                this.event.location = res.slug;
            }
        });
        modal.present();
    }

    public newOrganizer(): void {
        let modal: Modal = this.modalCtrl.create("AddOrganizerPage");
        modal.onDidDismiss((res: any) => {
            console.log("Created:", res);
            if (res) {
                this.organizers.push(res);
                this.event.organizer = res.slug;
            }
        });
        modal.present();
    }

    private resetEvent(): void {
        this.event.endTime = new Date();
        this.event.startTime = new Date();
        this.event.types = [];
        this.event.location = undefined;
        this.event.title = "";
        this.event.content = "";
        this.event.image = undefined;
        this.event.allDay = false;
        this.event.kraje[0] = undefined;
        if (this.user.roles[0] == UserRole.ADMINISTRATOR) {
            this.event.organizer = undefined;
        }
    }

    private async saveAutoFills(): Promise<void>
    {
        await this.storage.set("eventLocation", this.event.location);
        await this.storage.set("eventKraj", this.event.kraje[0]);
        await this.storage.set("eventOrganizer", this.event.organizer);
    }

}
