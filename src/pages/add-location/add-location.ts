import { Component } from "@angular/core";
import { IonicPage } from "ionic-angular";
import {
    Alert, AlertController, Loading, LoadingController, NavController, NavParams,
    ViewController
} from "ionic-angular";

import { TermsProvider } from "../../providers/terms-provider";

/**
 * Generated class for the AddLocation page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: "page-add-location",
    templateUrl: "add-location.html",
})
export class AddLocationPage {

    public name: string;
    public address: string;
    public latitude: number;
    public longitude: number;
    public description: string;

    constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private termsProv: TermsProvider) {
    }

    public ionViewDidLoad(): void {
        console.log("ionViewDidLoad AddLocation");
    }

    public dismiss(): void {
        this.viewCtrl.dismiss();
    }

    public async create(): Promise<any> {
        if (!this.name || this.name.trim() == "") {
            this.alertCtrl.create({
                title: "Chyba",
                message: "Vyplňte prosím název lokace.",
                buttons: ["OK"]
            }).present();
        } else {
            let loading: Loading = this.loadingCtrl.create({
                content: "Vytvářím místo konání"
            });
            loading.present();
            try {
                let res: any = await this.termsProv.addLocation(this.name, this.address, this.description, this.latitude, this.longitude);
                loading.dismiss();
                this.viewCtrl.dismiss(res.term);
            } catch (res) {
                loading.dismiss();
                let alert: Alert = this.alertCtrl.create({
                    title: "Chyba",
                    message: "Při přidávání místa konání nastala chyba, zkuste to prosím později.",
                    buttons: ["OK"]
                });
                try {
                    res = res.json();
                    if (res.error == "Term already exists.") {
                        this.alertCtrl.create({
                            title: "Chyba",
                            message: "Místo konání s tímto názvem již existuje",
                            buttons: ["OK"]
                        }).present();
                    } else {
                        alert.present();
                    }

                } catch (ex) {
                    alert.present();
                }
            }
        }
    }

}
