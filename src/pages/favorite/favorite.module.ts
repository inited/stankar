import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { TruncatePipe2 } from "../../pipes/truncate2";
import { FavoritePage } from "./favorite";

@NgModule({
    declarations: [
        FavoritePage,
        TruncatePipe2
    ],
    imports: [
        IonicPageModule.forChild(FavoritePage),
    ],
})
export class FavoritePageModule {}
