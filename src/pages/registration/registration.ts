import { Component } from "@angular/core";
import { IonicPage } from "ionic-angular";
import { AlertController, Loading, LoadingController, NavController, NavParams } from "ionic-angular";
import { UsersProvider } from "../../providers/users-provider";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { BaseUrl } from "../../app/app.module";

/*
 Generated class for the Registration page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: "page-registration",
    templateUrl: "registration.html"
})
export class RegistrationPage {

    constructor(public inAppBrowser: InAppBrowser) {
    }

    public ionViewDidLoad(): void {
        console.log("ionViewDidLoad RegistrationPage");
    }

    public openWeb(url: string): void {
        this.inAppBrowser.create(BaseUrl + url, "_system");
    }

}
