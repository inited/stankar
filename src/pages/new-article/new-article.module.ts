import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { FileUploadModule } from "ng2-file-upload";
import { NewArticlePage } from "./new-article";

@NgModule({
    declarations: [
        NewArticlePage,
    ],
    imports: [
        FileUploadModule,
        IonicPageModule.forChild(NewArticlePage),
    ],
    exports: [
        NewArticlePage
    ]
})
export class NewArticlePageModule {}
