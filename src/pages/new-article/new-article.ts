import { Component } from "@angular/core";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { ImagePicker } from "@ionic-native/image-picker";
import { IonicPage } from "ionic-angular";
import { AlertController, Loading, LoadingController, NavController, NavParams } from "ionic-angular";
import { FileItem, FileUploader } from "ng2-file-upload";

import { Article } from "../../model/Article";
import { User } from "../../model/User";
import { UserLevel } from "../../model/UserLevel";
import { UserRole } from "../../model/UserRole";
import { PostsProvider } from "../../providers/posts-provider";
import { UsersProvider } from "../../providers/users-provider";

/*
 Generated class for the NewArticle page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: "page-new-article",
    templateUrl: "new-article.html"
})
export class NewArticlePage {

    public article: Article = {
        id: undefined,
        header: "",
        rawHeader: "",
        rawText: "",
        text: undefined,
        image: "",
        date: undefined,
        author: "",
        categories: [],
        favorite: false,
        actionInProgress: false
    };
    public previewUrl: SafeUrl;
    public uploader: FileUploader = new FileUploader({url: ""});
    public selectedFile: any = undefined;
    public user: User;

    constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public postsProv: PostsProvider, private sanitizer: DomSanitizer, private loadingCtrl: LoadingController, public usersProv: UsersProvider, private imagePicker: ImagePicker) {
        this.uploader.onAfterAddingFile = (fileItem: FileItem): void => {
            this.fileSelected(fileItem);
        };
        this.previewUrl = sanitizer.bypassSecurityTrustUrl("assets/no-image.jpg");
        this.user = usersProv.user;

        if (this.user.roles.indexOf(UserRole.ADMINISTRATOR) != -1) {
            this.article.categories.push("clanky");
        } else {
            if (this.user.levels.indexOf(UserLevel.PORADATELE) != -1) {
                this.article.categories.push("upozorneni-poradatele");
            } else {
                this.article.categories.push("upozorneni-stankare");
            }
        }
        console.log(this.user);
    }

    public ionViewDidLoad(): void {
        console.log("ionViewDidLoad NewArticlePage");
    }

    public create(): void {
        if (this.article.rawHeader.trim() == "" || this.article.rawText.trim() == "") {
            this.alertCtrl.create({
                title: "Chybí údaje",
                message: "Vyplňte prosím titulek a obsah příspěvku.",
                buttons: ["OK"]
            }).present();
        } else {
            let loading: Loading = this.loadingCtrl.create({
                content: "Vytvářím příspěvek"
            });
            loading.present();
            this.postsProv.addArticle(this.article, this.selectedFile, this.uploader).then((res: any) => {
                loading.dismiss();
                this.navCtrl.setRoot("ArticlesPage");
            }).catch((res: any) => {
                loading.dismiss();
                if (res.status == 403) {
                    this.alertCtrl.create({
                        title: "Chyba",
                        message: "Nemáte právo přidávat nové články, kontaktujte prosím administrátora webu.",
                        buttons: ["OK"]
                    }).present();
                } else if (!res.status) {
                    this.alertCtrl.create({
                        title: "Chyba",
                        message: "Nastala chyba při připojování k internetu, zkontrolujte si prosím vaše spojení.",
                        buttons: ["OK"]
                    }).present();
                } else {
                    let data: any = res.json();
                    let error: string = data.error;
                    if (data.error == "Not logged in") {
                        error = "Nejste přihlášen.";
                        this.usersProv.logout();
                        this.navCtrl.setRoot("ArticlesPage");
                    }
                    this.alertCtrl.create({
                        title: "Chyba",
                        message: error,
                        buttons: ["OK"]
                    }).present();
                }
            });
        }
    }

    public showFileUpload(input: HTMLInputElement): void {
        this.imagePicker.getPictures({
            maximumImagesCount: 1,
            width: 1024,
            height: 1024
        }).then((results: Array<any>) => {
            if (!results[0] || results[0] == "") {
                this.previewUrl = this.sanitizer.bypassSecurityTrustUrl("assets/no-image.jpg");
                this.selectedFile = undefined;
            } else {
                this.previewUrl = this.sanitizer.bypassSecurityTrustUrl(results[0]);
                this.selectedFile = results[0];
            }
            console.log(results);
        }).catch((res: any) => {
            input.click();
        });
    }

    public fileSelected(fileItem: FileItem): void {
        if (fileItem.file.size > 2000000) {

        }
        this.selectedFile = fileItem;
        this.selectedFile.alias = "attachment";
        console.log(fileItem);
        this.previewUrl = this.sanitizer.bypassSecurityTrustUrl((window.URL.createObjectURL(fileItem._file)));
    }

}
