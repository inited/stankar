import { Component } from "@angular/core";
import { Response } from "@angular/http";
import { IonicPage } from "ionic-angular";
import {
    Alert, AlertController, Loading, LoadingController, NavController, NavParams,
    ViewController
} from "ionic-angular";

import { TermsProvider } from "../../providers/terms-provider";

/**
 * Generated class for the AddOrganizer page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: "page-add-organizer",
    templateUrl: "add-organizer.html",
})
export class AddOrganizerPage {

    public name: string;
    public contact: string;
    public exlink: string;

    constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private termsProv: TermsProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    }

    public ionViewDidLoad(): void {
        console.log("ionViewDidLoad AddOrganizer");
    }

    public dismiss(): void {
        this.viewCtrl.dismiss();
    }

    public async create(): Promise<any> {
        if (!this.name || this.name.trim() == "" || !this.contact || this.contact.trim() == "") {
            this.alertCtrl.create({
                title: "Chyba",
                message: "Vyplňte prosím název a kontakt.",
                buttons: ["OK"]
            }).present();
        } else {
            let loading: Loading = this.loadingCtrl.create({
                content: "Vytvářím pořadatele"
            });
            loading.present();
            try {
                let res: any = await this.termsProv.addOrganizer(this.name, this.contact, this.exlink);
                loading.dismiss();
                this.viewCtrl.dismiss(res.term);
            } catch (ex) {
                loading.dismiss();
                let alert: Alert = this.alertCtrl.create({
                    title: "Chyba",
                    message: "Při přidávání pořadatele nastala chyba, zkuste to prosím později.",
                    buttons: ["OK"]
                });
                try {
                    ex = ex.json();
                    if (ex.error == "Term already exists.") {
                        this.alertCtrl.create({
                            title: "Chyba",
                            message: "Pořadatel s tímto jménem již existuje",
                            buttons: ["OK"]
                        }).present();
                    } else {
                        alert.present();
                    }

                } catch (ex) {
                    alert.present();
                }
            }
        }
    }

}
