import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { AddOrganizerPage } from "./add-organizer";

@NgModule({
    declarations: [
        AddOrganizerPage,
    ],
    imports: [
        IonicPageModule.forChild(AddOrganizerPage),
    ],
    exports: [
        AddOrganizerPage
    ]
})
export class AddOrganizerPageModule {}
